## Prerequisite

Node.js 10.9+

Git 2.15+

Postman 6.2+ for verification



## Install Git

The app uses [simple-git](https://www.npmjs.com/package/simple-git) nodejs library which delegates to the `git` command, so Git must be installed.



## Config app

- config/default.json: defines the default configuration for the app
- config/test.json: defines the configuration used for unit tests
- config/custom-environment-variables.json: defines the environment variables to override config
- config/ext-config.json: used to locally store the external config

App configuration:

| Name             | Environment        | Description                                                  |
| ---------------- | ------------------ | ------------------------------------------------------------ |
| port             | PORT               | The server port. Default to 3000                             |
| apiVersion       | API_VERSION        | The api version, used to construct rest api path             |
| logLevel         | LOG_LEVEL          | The log level                                                |
| reposFolder      | REPOS_FOLDER       | The folder to clone repos into                               |
| repoMetadataFile | REPO_METADATA_FILE | The file name used to save repo metadata. Default to ".yaml-editor-repo-metadata" |
| jwtSecret        | JWT_SECRET         | The JWT secret                                               |
| jwtExpiresIn     | JWT_EXPIRES_IN     | The JWT token expires in, like "1m", "2.5 hrs", "2 days". Default to 12 hours. |
| encryptKey       | ENCRYPT_KEY        | The key used to encrypt password                             |
| encryptSalt      | ENCRYPT_SALT       | The salt used to encrypt password                            |
| maxPayloadSize   | MAX_PAYLOAD_SIZE   | The max size of JSON request payload. Default to 1M.         |



When app starts, if `EXTERNAL_CONFIG_URL` env value exists, it will load external configuration from that URL, otherwise the locally stored `config/ext-config.json` will be used:

| Name         | Description                              |
| ------------ | ---------------------------------------- |
| version      | The app version                          |
| branchName   | The branch to checkout when cloning repo |
| environments | The environments                         |



(For simple verification, you can just use the default config)



## NPM commands

```bash
# Install dependencies
npm install

# Clean app, the "build" and "coverage" folders will be removed
npm run clean

# Build app, the typescripts within "src" and "test" folders
# will be built into "build" folder
npm run build

# Lint code
npm run lint

# Run unit tests, the coverage report will be generated in "coverage" folder
npm test

# Start app
npm start
```



The app started at http://localhost:3000 by default



## Unit Tests

Unit tests will mock the git behaviour of clone/pull/push, you don't need change config to run the tests.

See result: http://take.ms/DhDFW



## Verification

At first init this project repo locally, it is required to verify `/healthcheck` api (if this project is cloned from git repo, then this step can be skipped):

```bash
# Within the project root dir:
git init
git add .
git commit -m 'init'
```



Then setup a local http server to serve the external config, it is required to verify `/refreshConfiguration` api:

```bash
# Within the project root dir:
npm i -g http-server

# Start http-server:
http-server
```



Import collection file `doc/yaml-config-rest-api.postman_collection.json` and environment file `doc/yaml-config.postman_environment.json` to Postman.

Then config following Postman env values to your own test repo url and your own username/password:

- **repoURL**
- **username**
- **Password**

**Please note, your test repo should have a branch named "admin", because by default the "admin" branch will be checkout when cloning repo.**

(*The **token** and **repoName** are auto updated after calling to `/accessRepo`, you don't need manually change them*).



- /healthcheck: http://take.ms/15gQS
- /environments: http://take.ms/cytAe
- /log: http://take.ms/Rjs0S
- /refreshConfiguration: http://take.ms/ryUYU



Then you need run `/accessRepo` at first, which will clone the repo and grant access token (the repo is cloned under `repos` folder by default). After that, you can create/list/update/get/delete file:

- accessRepo: http://take.ms/CYIV9
- createFile: http://take.ms/yzZru
- listFiles: http://take.ms/CBmVF
- updateFile: http://take.ms/QxzRg
- getFile: http://take.ms/5zx8G
- deleteFile: http://take.ms/Ijfim
- userTypeAhead: https://snag.gy/0ykYpa.jpg