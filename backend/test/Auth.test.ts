/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file defines tests for auth security.
 *
 * @author TCSCODER
 * @version 1.0
 */
import { assert } from 'chai';
import * as config from 'config';
import * as fs from 'fs-extra';
import * as Hapi from 'hapi';
import * as jwt from 'jsonwebtoken';

import { start } from '../src/server';

const tokenPayload: any = {
  username: 'TestUser',
  localPath: 'TestUser-test-admin',
};

/**
 * Sign JWT token.
 * @param payload The token payload
 * @param expiresIn The seconds token will expire in
 * @returns JWT token
 */
function signToken(expiresIn: number) {
  tokenPayload.iat = Math.floor(Date.now() / 1000);
  tokenPayload.exp = tokenPayload.iat + expiresIn;
  return jwt.sign(tokenPayload, config.jwtSecret);
}

describe('Auth Tests', () => {
  let server: Hapi.Server;

  before(async () => {
    server = await start();
  });

  after(async () => {
    await server.stop();
    fs.removeSync(config.reposFolder);
  });

  beforeEach(async () => {
    const repoPath = `${config.reposFolder}/${tokenPayload.localPath}`;
    fs.removeSync(repoPath);
  });

  describe('GET /repos/{{repoName}}/files', () => {
    test('GET', `/api/${config.apiVersion}/repos/repoName/files`);
  });

  describe('GET /repos/{{repoName}}/files/{{filepath}}', () => {
    test('GET', `/api/${config.apiVersion}/repos/repoName/files/file`);
  });

  describe('POST /repos/{{repoName}}/files/{{filepath}}', () => {
    test('POST', `/api/${config.apiVersion}/repos/repoName/files/file`);
  });

  describe('PUT /repos/{{repoName}}/files/{{filepath}}', () => {
    test('PUT', `/api/${config.apiVersion}/repos/repoName/files/file`);
  });

  describe('DELETE /repos/{{repoName}}/files/{{filepath}}', () => {
    test('DELETE', `/api/${config.apiVersion}/repos/repoName/files/file`);
  });

  /**
   * Do tests.
   * @param method The http method
   * @param url The url
   */
  function test(method, url) {
    it('Miss access token, 401 error expected', async () => {
      const res = await server.inject({
        method,
        url,
      });

      assert.equal(res.statusCode, 401);
      assert.equal(JSON.parse(res.payload).message, 'Miss access token');
    });

    it('Invalid access token, 401 error expected', async () => {
      const res = await server.inject({
        method,
        url,
        headers: {
          authorization: 'invalid token',
        },
      });

      assert.equal(res.statusCode, 401);
      assert.equal(JSON.parse(res.payload).message, 'Invalid access token');
    });

    it('Expired access token, 401 error expected', async () => {
      const res = await server.inject({
        method,
        url,
        headers: {
          authorization: signToken(-1000),
        },
      });

      assert.equal(res.statusCode, 401);
      assert.equal(JSON.parse(res.payload).message, 'Expired access token');
    });

    it('Repo metadata not found, 401 error expected', async () => {
      const res = await server.inject({
        method,
        url,
        headers: {
          authorization: signToken(1000),
        },
      });

      assert.equal(res.statusCode, 401);
      assert.equal(JSON.parse(res.payload).message, 'Repo metadata not found');
    });

    it('Token not found in repo metadata, 401 error expected', async () => {
      const localRepoDir = `${config.reposFolder}/${tokenPayload.localPath}`;
      fs.outputJsonSync(`${localRepoDir}/${config.repoMetadataFile}`, {});

      const res = await server.inject({
        method,
        url,
        headers: {
          authorization: signToken(1000),
        },
      });

      assert.equal(res.statusCode, 401);
      assert.equal(JSON.parse(res.payload).message, 'Access token not found');
    });

    it('Repo metadata mismatch, 403 error expected', async () => {
      const token = signToken(1000);
      const localRepoDir = `${config.reposFolder}/${tokenPayload.localPath}`;
      fs.outputJsonSync(`${localRepoDir}/${config.repoMetadataFile}`, {
        token,
      });

      const res = await server.inject({
        method,
        url,
        headers: {
          authorization: token,
        },
      });

      assert.equal(res.statusCode, 403);
      assert.equal(JSON.parse(res.payload).message, 'Repo metadata mismatch, you are not allowed to access the repo');
    });

    it('Repo metadata mismatch, 403 error expected', async () => {
      const token = signToken(1000);
      const localRepoDir = `${config.reposFolder}/${tokenPayload.localPath}`;
      fs.outputJsonSync(`${localRepoDir}/${config.repoMetadataFile}`, {
        token,
        username: tokenPayload.username,
        localPath: tokenPayload.localPath,
      });

      const res = await server.inject({
        method,
        url,
        headers: {
          'authorization': token,
          'user-agent': 'chrome1',
        },
      });

      assert.equal(res.statusCode, 401);
      assert.equal(JSON.parse(res.payload).message, 'Same browser and machine should be used');
    });
  }

});
