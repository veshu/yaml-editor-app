/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file defines tests for helper.
 *
 * @author TCSCODER
 * @version 1.0
 */
import { assert } from 'chai';

import * as errors from '../src/utils/errors';
import * as helper from '../src/utils/helper';

describe('Helper Tests', () => {
  it('Encrypt/decrypt password', () => {
    const password = 'test password';
    const encrypted = helper.encrypt(password);
    assert.equal(helper.decrypt(encrypted), password);
  });

  it('Convert Git error', () => {
    let error = helper.convertGitError(new Error('remote: Invalid username or password'));
    assert.isTrue(error instanceof errors.NotAuthenticatedError);

    error = helper.convertGitError(new Error('remote: Unauthorized'));
    assert.isTrue(error instanceof errors.ForbiddenError);

    error = helper.convertGitError(new Error('remote: Forbidden'));
    assert.isTrue(error instanceof errors.ForbiddenError);

    error = helper.convertGitError(new Error('Network error'));
    assert.equal(error, error);
  });

});
