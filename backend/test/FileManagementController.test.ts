/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file defines tests for FileManagementController.
 *
 * @author TCSCODER
 * @version 1.0
 */
import * as path from 'path';

import { assert } from 'chai';
import * as config from 'config';
import * as fs from 'fs-extra';
import * as Hapi from 'hapi';
import * as jsYaml from 'js-yaml';
import * as simpleGit from 'simple-git/promise';
import * as TypeMoq from 'typemoq';

import { start } from '../src/server';
import MockGit from './MockGit';

const sampleJson = fs.readJsonSync(path.resolve(__dirname, '../../test/sample.json'));
const sampleYaml = fs.readFileSync(path.resolve(__dirname, '../../test/sample.yaml')).toString();

describe('FileManagementController Tests', () => {
  let server: Hapi.Server;
  let cloneError = false;
  let pushError = false;

  before(async () => {
    server = await start();
  });

  after(async () => {
    await server.stop();
    fs.removeSync(config.reposFolder);
  });

  beforeEach(() => {
    MockGit.reset();
    cloneError = false;
    pushError = false;
    fs.emptyDirSync(config.reposFolder);
  });

  /**
   * Access repo.
   * @returns request payload, repo path, repo name and response result
   */
  async function accessRepo() {
    const payload = {
      repoURL: 'https://bitbucket.org/testuser/testrepo',
      username: 'test@gmail.com',
      password: '123456',
      branch: Date.now() % 2 === 0 ? config.branchName : undefined,
    };

    const repoName = 'testuser-testrepo';
    const localPath = encodeURIComponent(`${payload.username}-${repoName}-${config.branchName}`);
    const repoPath = `${config.reposFolder}/${localPath}`;

    // Mock the push behavior
    MockGit.setup((x) => x.push(TypeMoq.It.isAny(), TypeMoq.It.isAny())).returns(() => {
      if (pushError) {
        return Promise.reject(new Error('Mock push error'));
      }
      return Promise.resolve(null);
    });

    // Mock the clone behavior by init a local repo
    MockGit
      .setup((x) => x.clone(TypeMoq.It.isAnyString(), TypeMoq.It.isAnyString(), TypeMoq.It.isAny()))
      .returns(async () => {
        fs.ensureDirSync(repoPath);
        fs.outputFileSync(`${repoPath}/readme.md`, 'Test repo');

        const git = simpleGit(repoPath);
        await git.init(false);
        await git.add('readme.md');
        await git.commit('Test init', 'readme.md', { '--author': `"${payload.username} <${payload.username}>"` });
        await git.branch([config.branchName]);
        await git.checkout(config.branchName);

        if (cloneError) {
          return Promise.reject(new Error('Mock clone error'));
        }
        return Promise.resolve('');
      });

    // These methods are locally and delegate to the real git command
    MockGit.setup((x) => x.log(TypeMoq.It.isAny())).returns((arg) => simpleGit(repoPath).log(arg));
    MockGit.setup((x) => x.checkout(TypeMoq.It.isAny())).returns((arg) => simpleGit(repoPath).checkout(arg));
    // This is a local only repo which has no upstream, so discard '@{u}'
    MockGit.setup((x) => x.reset(['--hard', '@{u}'])).returns(() => {
      return pushError ? simpleGit(repoPath).reset(['--hard', 'HEAD^']) : simpleGit(repoPath).reset(['--hard']);
    });
    MockGit.setup((x) => x.add(TypeMoq.It.isAny())).returns((arg) => simpleGit(repoPath).add(arg));
    MockGit.setup((x) => x.commit(TypeMoq.It.isAny(), TypeMoq.It.isAny(), TypeMoq.It.isAny()))
      .returns((message, files, options) => simpleGit(repoPath).commit(message, files, options));

    const res = await server.inject({
      method: 'POST',
      url: `/api/${config.apiVersion}/accessRepo`,
      payload,
    });

    let result;
    if (!cloneError) {
      assert.equal(res.statusCode, 200);
      result = JSON.parse(res.payload);
      assert.isNotEmpty(result.token);
      assert.isNotEmpty(result.validUntil);
      assert.equal(result.repoName, repoName);
    }

    return { repoPath, repoName, payload, accessResult: result };
  }

  describe('GET /repos/{{repoName}}/files', () => {

    it('Yaml files in repo root folder should be listed', async () => {
      const { repoPath, repoName, accessResult } = await accessRepo();

      fs.outputFileSync(`${repoPath}/test1.yaml`, 'a:b');
      fs.outputFileSync(`${repoPath}/test2.yml`, 'c:d');
      fs.outputFileSync(`${repoPath}/folder/test3.yaml`, 'e:f');
      fs.outputJsonSync(`${repoPath}/config.json`, {});

      const res = await server.inject({
        method: 'GET',
        url: `/api/${config.apiVersion}/repos/${repoName}/files`,
        headers: {
          authorization: accessResult.token,
        },
      });

      assert.equal(res.statusCode, 200);
      const result = JSON.parse(res.payload);
      assert.equal(result.length, 2);
      assert.equal(result[0].filename, 'test1.yaml');
      assert.equal(result[1].filename, 'test2.yml');
    });
  });

  describe('GET /repos/{{repoName}}/files/{{filepath}}', () => {

    it('Get yaml file, should be converted to json format', async () => {
      const { repoPath, repoName, accessResult } = await accessRepo();

      fs.copyFileSync(path.resolve(__dirname, '../../test/sample.yaml'), `${repoPath}/sample.yaml`);

      const res = await server.inject({
        method: 'GET',
        url: `/api/${config.apiVersion}/repos/${repoName}/files/sample.yaml`,
        headers: {
          authorization: accessResult.token,
        },
      });

      assert.equal(res.statusCode, 200);
      const result = JSON.parse(res.payload);
      assert.deepEqual(result, sampleJson);
    });

    it('Get yaml file, repo name mismatch, 400 error expected', async () => {
      const { accessResult } = await accessRepo();

      const res = await server.inject({
        method: 'GET',
        url: `/api/${config.apiVersion}/repos/NoSuchRepo/files/sample.yaml`,
        headers: {
          authorization: accessResult.token,
        },
      });

      assert.equal(res.statusCode, 400);
      const result = JSON.parse(res.payload);
      assert.equal(result.message, 'Repo name mismatch');
    });

    it('Get yaml file, file does not exist, 404 error expected', async () => {
      const { repoName, accessResult } = await accessRepo();

      const res = await server.inject({
        method: 'GET',
        url: `/api/${config.apiVersion}/repos/${repoName}/files/NoSuchFile.yaml`,
        headers: {
          authorization: accessResult.token,
        },
      });

      assert.equal(res.statusCode, 404);
      const result = JSON.parse(res.payload);
      assert.equal(result.message, 'NoSuchFile.yaml does not exist');
    });

  });

  describe('DELETE /repos/{{repoName}}/files/{{filepath}}', () => {

    it('Delete yaml file should success', async () => {
      const { repoPath, repoName, accessResult } = await accessRepo();

      fs.copyFileSync(path.resolve(__dirname, '../../test/sample.yaml'), `${repoPath}/sample.yaml`);

      const res = await server.inject({
        method: 'DELETE',
        url: `/api/${config.apiVersion}/repos/${repoName}/files/sample.yaml`,
        headers: {
          authorization: accessResult.token,
        },
      });

      assert.equal(res.statusCode, 204);
      assert.isTrue(!fs.existsSync(`${repoPath}/sample.yaml`));
    });

    it('Delete yaml file, repo name mismatch, 400 error expected', async () => {
      const { accessResult } = await accessRepo();

      const res = await server.inject({
        method: 'DELETE',
        url: `/api/${config.apiVersion}/repos/NoSuchRepo/files/sample.yaml`,
        headers: {
          authorization: accessResult.token,
        },
      });

      assert.equal(res.statusCode, 400);
      const result = JSON.parse(res.payload);
      assert.equal(result.message, 'Repo name mismatch');
    });

    it('Delete yaml file, file does not exist, 404 error expected', async () => {
      const { repoName, accessResult } = await accessRepo();

      const res = await server.inject({
        method: 'DELETE',
        url: `/api/${config.apiVersion}/repos/${repoName}/files/NoSuchFile.yaml`,
        headers: {
          authorization: accessResult.token,
        },
      });

      assert.equal(res.statusCode, 404);
      const result = JSON.parse(res.payload);
      assert.equal(result.message, 'NoSuchFile.yaml does not exist');
    });

  });

  describe('POST /repos/{{repoName}}/files/{{filepath}}', () => {

    it('Create yaml file should success', async () => {
      const { repoPath, repoName, accessResult } = await accessRepo();

      const res = await server.inject({
        method: 'POST',
        url: `/api/${config.apiVersion}/repos/${repoName}/files/sample.yaml`,
        headers: {
          authorization: accessResult.token,
        },
        payload: {
          message: 'Test create yaml file',
          fileContent: sampleJson,
        },
      });

      assert.equal(res.statusCode, 204);
      assert.deepEqual(jsYaml.safeLoad(fs.readFileSync(`${repoPath}/sample.yaml`).toString()),
        jsYaml.safeLoad(sampleYaml));
    });

    it('Create yaml file, push fails, should revert to last commit', async () => {
      pushError = true;
      const { repoPath, repoName, accessResult } = await accessRepo();

      const res = await server.inject({
        method: 'POST',
        url: `/api/${config.apiVersion}/repos/${repoName}/files/sample.yaml`,
        headers: {
          authorization: accessResult.token,
        },
        payload: {
          message: 'Test create yaml file',
          fileContent: sampleJson,
        },
      });

      assert.equal(res.statusCode, 500);
      assert.isTrue(!fs.existsSync(`${repoPath}/sample.yaml`));
    });

    it('Create yaml file, repo name mismatch, 400 error expected', async () => {
      const { accessResult } = await accessRepo();

      const res = await server.inject({
        method: 'POST',
        url: `/api/${config.apiVersion}/repos/NoSuchRepo/files/sample.yaml`,
        headers: {
          authorization: accessResult.token,
        },
        payload: {
          message: 'Test create yaml file',
          fileContent: sampleJson,
        },
      });

      assert.equal(res.statusCode, 400);
      const result = JSON.parse(res.payload);
      assert.equal(result.message, 'Repo name mismatch');
    });

    it('Create yaml file, file already exists, 409 error expected', async () => {
      const { repoPath, repoName, accessResult } = await accessRepo();
      fs.copyFileSync(path.resolve(__dirname, '../../test/sample.yaml'), `${repoPath}/sample.yaml`);

      const res = await server.inject({
        method: 'POST',
        url: `/api/${config.apiVersion}/repos/${repoName}/files/sample.yaml`,
        headers: {
          authorization: accessResult.token,
        },
        payload: {
          message: 'Test create yaml file',
          fileContent: sampleJson,
        },
      });

      assert.equal(res.statusCode, 409);
      const result = JSON.parse(res.payload);
      assert.equal(result.message, 'sample.yaml already exists');
    });

  });

  describe('PUT /repos/{{repoName}}/files/{{filepath}}', () => {

    it('Update yaml file should success', async () => {
      const { repoPath, repoName, accessResult } = await accessRepo();

      // Create yaml file at first
      await server.inject({
        method: 'POST',
        url: `/api/${config.apiVersion}/repos/${repoName}/files/sample.yaml`,
        headers: {
          authorization: accessResult.token,
        },
        payload: {
          message: 'Test create yaml file',
          fileContent: sampleJson,
        },
      });

      // Update it
      const res = await server.inject({
        method: 'PUT',
        url: `/api/${config.apiVersion}/repos/${repoName}/files/sample.yaml`,
        headers: {
          authorization: accessResult.token,
        },
        payload: {
          message: 'Test update yaml file',
          fileContent: { a: 'b' },
        },
      });

      assert.equal(res.statusCode, 204);
      assert.equal(fs.readFileSync(`${repoPath}/sample.yaml`).toString().trim(), 'a: b');
    });

    it('Update yaml file, push fails, should revert to last commit', async () => {
      const { repoPath, repoName, accessResult } = await accessRepo();

      // Create yaml file at first
      await server.inject({
        method: 'POST',
        url: `/api/${config.apiVersion}/repos/${repoName}/files/sample.yaml`,
        headers: {
          authorization: accessResult.token,
        },
        payload: {
          message: 'Test create yaml file',
          fileContent: sampleJson,
        },
      });

      // Update it
      pushError = true;
      const res = await server.inject({
        method: 'PUT',
        url: `/api/${config.apiVersion}/repos/${repoName}/files/sample.yaml`,
        headers: {
          authorization: accessResult.token,
        },
        payload: {
          message: 'Test update yaml file',
          fileContent: { a: 'b' },
        },
      });

      assert.equal(res.statusCode, 500);
      assert.deepEqual(jsYaml.safeLoad(fs.readFileSync(`${repoPath}/sample.yaml`).toString()),
        jsYaml.safeLoad(sampleYaml));
    });

    it('Update yaml file, repo name mismatch, 400 error expected', async () => {
      const { accessResult } = await accessRepo();

      const res = await server.inject({
        method: 'PUT',
        url: `/api/${config.apiVersion}/repos/NoSuchRepo/files/sample.yaml`,
        headers: {
          authorization: accessResult.token,
        },
        payload: {
          message: 'Test update yaml file',
          fileContent: sampleJson,
        },
      });

      assert.equal(res.statusCode, 400);
      const result = JSON.parse(res.payload);
      assert.equal(result.message, 'Repo name mismatch');
    });

    it('Update yaml file, file does not exist, 404 error expected', async () => {
      const { repoName, accessResult } = await accessRepo();

      const res = await server.inject({
        method: 'PUT',
        url: `/api/${config.apiVersion}/repos/${repoName}/files/sample.yaml`,
        headers: {
          authorization: accessResult.token,
        },
        payload: {
          message: 'Test create yaml file',
          fileContent: sampleJson,
        },
      });

      assert.equal(res.statusCode, 404);
      const result = JSON.parse(res.payload);
      assert.equal(result.message, 'sample.yaml does not exist');
    });

  });

  describe('POST /accessRepo', () => {

    async function sleep(time) {
      return new Promise((resolve) => {
        setTimeout(() => resolve(), time * 1000);
      });
    }

    it('Access repo should success', async () => {
      const { repoName, payload, accessResult } = await accessRepo();

      await sleep(1);

      // Access repo again
      const res = await server.inject({
        method: 'POST',
        url: `/api/${config.apiVersion}/accessRepo`,
        payload,
      });

      // assert.equal(res.statusCode, 200);
      const result = JSON.parse(res.payload);
      assert.isNotEmpty(result.token);
      assert.isNotEmpty(result.validUntil);
      assert.notEqual(result.token, accessResult.token, 'Token should change');
      assert.notEqual(result.validUntil, accessResult.validUntil, 'Token valid time should change');
      assert.equal(result.repoName, repoName);
      const loggedInUsersMetaFile = `${config.reposFolder}/${config.loggedInUsersMetaFile}`;
      assert.equal(fs.existsSync(loggedInUsersMetaFile), true);
      const users = fs.readJsonSync(loggedInUsersMetaFile);
      assert.equal(users.length, 1);
      assert.equal(users[0], payload.username);
    });

    it('Repo metadata does not exist, should clone again', async () => {
      const { repoPath, repoName, payload, accessResult } = await accessRepo();

      fs.removeSync(`${repoPath}/${config.repoMetadataFile}`);

      await sleep(1);

      // Access repo again
      const res = await server.inject({
        method: 'POST',
        url: `/api/${config.apiVersion}/accessRepo`,
        payload,
      });

      assert.equal(res.statusCode, 200);
      const result = JSON.parse(res.payload);
      assert.isNotEmpty(result.token);
      assert.isNotEmpty(result.validUntil);
      assert.notEqual(result.token, accessResult.token, 'Token should change');
      assert.notEqual(result.validUntil, accessResult.validUntil, 'Token valid time should change');
      assert.equal(result.repoName, repoName);
    });

    it('Repo metadata file corrupted, 401 error expected', async () => {
      const { repoPath, payload } = await accessRepo();

      fs.writeFileSync(`${repoPath}/${config.repoMetadataFile}`, '{corrupted');

      // Access repo again
      const res = await server.inject({
        method: 'POST',
        url: `/api/${config.apiVersion}/accessRepo`,
        payload,
      });

      assert.equal(res.statusCode, 401);
      const result = JSON.parse(res.payload);
      assert.equal(result.message, 'Repo metadata file corruption');
      assert.isTrue(!fs.existsSync(`${repoPath}/${config.repoMetadataFile}`));
    });

    it('Error while cloning, repo dir should be removed', async () => {
      cloneError = true;
      const { repoPath, payload } = await accessRepo();

      const res = await server.inject({
        method: 'POST',
        url: `/api/${config.apiVersion}/accessRepo`,
        payload,
      });
      assert.equal(res.statusCode, 500);
      assert.isTrue(!fs.existsSync(repoPath));
    });
  });

});
