/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */
/* tslint:disable:max-classes-per-file */

/**
 * This file defines the errors classes.
 *
 * @author TCSCODER
 * @version 1.0
 */

class BaseError extends Error {

  /**
   * The error cause.
   */
  public readonly cause: Error;

  /**
   * The http status code.
   */
  public readonly statusCode: number;

  /**
   * Constructor with message, status code and cause.
   * @param message The error message
   * @param statusCode The http status code
   * @param cause The error cause
   */
  constructor(message: string, statusCode: number, cause?: Error) {
    super(message);
    this.cause = cause;
    this.statusCode = statusCode;
  }
}

// BAD REQUEST - if there was problem with the request
// (e.g. malformed or some parameters are missing).
export class BadRequestError extends BaseError {
  /**
   * Constructor with message and cause.
   * @param message The error message
   * @param cause The error cause
   */
  constructor(message: string, cause?: Error) {
    super(message, 400, cause);
  }
}

// NOT AUTHORIZED - if the request didn't bear authentication information
// or the authentication information is invalid.
export class NotAuthenticatedError extends BaseError {
  /**
   * Constructor with message and cause.
   * @param message The error message
   * @param cause The error cause
   */
  constructor(message: string, cause?: Error) {
    super(message, 401, cause);
  }
}

// FORBIDDEN - if the requesting user didn't have permission to perform the requested operation
export class ForbiddenError extends BaseError {
  /**
   * Constructor with message and cause.
   * @param message The error message
   * @param cause The error cause
   */
  constructor(message: string, cause?: Error) {
    super(message, 403, cause);
  }
}

// NOT FOUND - if the resource in request did not exist
export class NotFoundError extends BaseError {
  /**
   * Constructor with message and cause.
   * @param message The error message
   * @param cause The error cause
   */
  constructor(message: string, cause?: Error) {
    super(message, 404, cause);
  }
}

// CONFLICT - if the resource already exists
export class ConflictError extends BaseError {
  /**
   * Constructor with message and cause.
   * @param message The error message
   * @param cause The error cause
   */
  constructor(message: string, cause?: Error) {
    super(message, 409, cause);
  }
}
