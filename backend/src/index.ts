/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file defines the app entry point.
 *
 * @author TCSCODER
 * @version 1.0
 */
import { start } from './server';

start();
