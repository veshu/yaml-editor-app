/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file defines the Hapi server.
 *
 * @author TCSCODER
 * @version 1.0
 */
import * as path from 'path';

import * as config from 'config';
import * as fs from 'fs-extra';
import * as Hapi from 'hapi';
import * as Inert from 'inert';
import * as jwt from 'jsonwebtoken';
import * as _ from 'lodash';

import routes from './routes';
import * as errors from './utils/errors';
import * as helper from './utils/helper';

import Cache from './utils/cache';

/**
 * Start Hapi server.
 * @returns Hapi server
 */
export async function start() {

  await helper.loadExtConfig(process.env.EXTERNAL_CONFIG_URL);

  // Create repos folder
  config.reposFolder = path.resolve(config.reposFolder);
  fs.ensureDirSync(config.reposFolder);
  helper.logger.info(`Use repos folder: ${config.reposFolder}`);

  // cache the users
  const loggedInUsersMetaFile = `${config.reposFolder}/${config.loggedInUsersMetaFile}`;
  if (fs.existsSync(loggedInUsersMetaFile)) {
    Cache.set('users', fs.readJsonSync(loggedInUsersMetaFile));
  } else {
    Cache.set('users', []);
  }

  // Create Hapi server
  const server = new Hapi.Server({
    port: config.port,
    routes: {
      cors: {
        origin: ['*'],
      },
      payload: {
        allow: ['application/json'],
        maxBytes: config.maxPayloadSize,
        multipart: false,
      },
    },
  });

  await server.register(Inert);

  server.route({
    method: 'GET',
    path: '/{path*}',
    handler: {
      directory: {
        path: './public',
        listing: false,
        index: true,
      },
    },
  });

  // Load routes
  const controllers = {};

  _.each(routes, (route) => {
    if (!controllers[route.controller]) {
      controllers[route.controller] = require(`./controllers/${route.controller}`);
    }
    server.route({
      handler: async (request, h) => {
        try {
          if (route.auth) {
            let token = request.headers.authorization;
            if (!token) {
              throw new errors.NotAuthenticatedError('Miss access token');
            }
            token = token.replace(/^Bearer /, '');

            // Verify and decode token payload
            let tokenPayload;
            try {
              tokenPayload = jwt.verify(token, config.jwtSecret);
            } catch (jwtErr) {
              throw new errors.NotAuthenticatedError(
                jwtErr.name === 'TokenExpiredError' ? 'Expired access token' : 'Invalid access token', jwtErr);
            }

            // Find stored repo metadata
            const localRepoDir = `${config.reposFolder}/${tokenPayload.localPath}`;
            const repoMetadataFile = `${localRepoDir}/${config.repoMetadataFile}`;
            if (!fs.existsSync(repoMetadataFile)) {
              throw new errors.NotAuthenticatedError('Repo metadata not found');
            }

            // Check with stored repo metadata to verify user having access to the repo
            tokenPayload.token = token;
            const storedRepoMetadata =
              helper.checkRepoAccess(tokenPayload, repoMetadataFile, ['username', 'localPath', 'token']);

            // verify same browser request
            if (request.info.remoteAddress !== tokenPayload.ip ||
              request.headers['user-agent'] !== tokenPayload.userAgent) {
              throw new errors.NotAuthenticatedError('Same browser and machine should be used');
            }

            // Set the repo metadata as auth credentials
            request.auth.isAuthorized = true;
            request.auth.isAuthenticated = true;
            request.auth.credentials = _.pick(storedRepoMetadata,
              ['repoURL', 'username', 'password', 'repoName', 'branch', 'localPath']);
          }

          const result = await controllers[route.controller][route.function](request, h);
          return h.response(result).code(result ? 200 : 204); // 204 for No Content
        } catch (err) {
          helper.logFullError(err, `${route.controller}#${route.function}`);
          return h.response({ message: _.defaultTo(err.message, 'An internal server error occurred') })
            .code(err.statusCode || err.status || 500);
        }
      },
      method: route.method,
      path: `/api/${config.apiVersion}${route.path}`,
    });
  });

  await server.start();
  helper.logger.info(`Hapi server running at: ${config.port}`);

  return server;
}
