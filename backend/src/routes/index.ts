/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file defines the routers.
 *
 * @author TCSCODER
 * @version 1.0
 */
export default [
  {
    method: 'POST',
    path: '/log',
    controller: 'MaintenanceController',
    function: 'log',
  },
  {
    method: 'GET',
    path: '/healthcheck',
    controller: 'MaintenanceController',
    function: 'healthcheck',
  },
  {
    method: 'POST',
    path: '/refreshConfiguration',
    controller: 'MaintenanceController',
    function: 'refreshConfiguration',
  },
  {
    method: 'GET',
    path: '/environments',
    controller: 'MaintenanceController',
    function: 'getEnvironments',
  },
  {
    method: 'GET',
    path: '/userTypeAhead',
    controller: 'MaintenanceController',
    function: 'getUserTypeAhead',
  },
  {
    method: 'POST',
    path: '/accessRepo',
    controller: 'FileManagementController',
    function: 'accessRepo',
  },
  {
    method: 'GET',
    path: '/repos/{repoName}/files',
    controller: 'FileManagementController',
    function: 'listFiles',
    auth: true,
  },
  {
    method: 'GET',
    path: '/repos/{repoName}/files/{filepath}',
    controller: 'FileManagementController',
    function: 'getFile',
    auth: true,
  },
  {
    method: 'POST',
    path: '/repos/{repoName}/files/{filepath}',
    controller: 'FileManagementController',
    function: 'createFile',
    auth: true,
  },
  {
    method: 'PUT',
    path: '/repos/{repoName}/files/{filepath}',
    controller: 'FileManagementController',
    function: 'updateFile',
    auth: true,
  },
  {
    method: 'DELETE',
    path: '/repos/{repoName}/files/{filepath}',
    controller: 'FileManagementController',
    function: 'deleteFile',
    auth: true,
  },
];
