/**
 * This file defines the file management controller.
 *
 * @author TCSCODER
 * @version 1.0
 */
import * as Hapi from 'hapi';

import FileManagementService from '../services/FileManagementService';

/**
 * Access/clone remote repository and receive the security token to be used in subsequent requests.
 * @param request Hapi request
 * @returns Security token of repo
 */
export async function accessRepo(request: Hapi.Request) {
  const requestInfo = {
    userAgent: request.headers['user-agent'],
    ip: request.info.remoteAddress,
  };
  return await FileManagementService.accessRepo(request.payload, requestInfo);
}

/**
 * List the yaml files for the repo. Only yaml file in root folder are listed.
 * @param request Hapi request
 * @returns yaml files in root folder of the repo
 */
export async function listFiles(request: Hapi.Request) {
  return await FileManagementService.listFiles(request.auth.credentials, request.params);
}

/**
 * Get the content of the file within the given location.
 * @param request Hapi request
 */
export async function getFile(request: Hapi.Request) {
  return await FileManagementService.getFile(request.auth.credentials, request.params);
}

/**
 * Creates the file within the (local) repository on server and commits it to the remote origin.
 * @param request Hapi request
 */
export async function createFile(request: Hapi.Request) {
  await FileManagementService.saveFile(
    request.auth.credentials, request.params, request.payload, false);
}

/**
 * Updates the file within the (local) repository on server and commits it to the remote origin.
 * @param request Hapi request
 */
export async function updateFile(request: Hapi.Request) {
  await FileManagementService.saveFile(
    request.auth.credentials, request.params, request.payload, true);
}

/**
 * Delete the file within the given location from local and remote repos.
 * @param request Hapi request
 */
export async function deleteFile(request: Hapi.Request) {
  await FileManagementService.deleteFile(request.auth.credentials, request.params);
}
