/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file defines the wrapper of SimpleGit.
 *
 * @author TCSCODER
 * @version 1.0
 */
import * as simpleGit from 'simple-git/promise';

export default { Git: simpleGit };
