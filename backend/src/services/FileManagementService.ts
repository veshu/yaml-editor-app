/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file defines the file management service.
 *
 * @author TCSCODER
 * @version 1.0
 */
import * as path from 'path';

import * as config from 'config';
import * as fs from 'fs-extra';
import * as Joi from 'joi';
import * as jwt from 'jsonwebtoken';
import * as _ from 'lodash';
import * as ms from 'ms';
import * as simpleGit from 'simple-git/promise';
import { URL } from 'url';

import Cache from '../utils/cache';

import * as errors from '../utils/errors';
import * as helper from '../utils/helper';
import * as Yaml from '../utils/yaml';
import GitWrapper from './GitWrapper';

// Joi schema for file params
const fileParamsSchema = Joi.object().keys({
  filepath: Joi.string().regex(/\.[y|Y][a|A]?[m|M][l|L]$/)
    .error(new errors.BadRequestError('only YAML files are supported')).required(),
  repoName: Joi.string().required(),
}).required();

// Joi schema for credentials
const credentialsSchema = Joi.object().keys({
  repoURL: Joi.string().uri().required(),
  username: Joi.string().required(),
  password: Joi.string().required(),
  repoName: Joi.string().required(),
  branch: Joi.string().required(),
  localPath: Joi.string().required(),
}).required();

/**
 * Clone remote repo and obtain request access token.
 * By default 'admin' branch is clone, but optional branch parameter is also supported.
 * @param requestBody The request body
 * @param requestInfo The request info
 * @returns access token
 */
async function accessRepo(requestBody, requestInfo) {
  const repoMetadata = {
    ...requestBody,
    // Remove '.git' from the end of repo url
    repoURL: requestBody.repoURL.replace(/\.git$/, ''),
    // Default branch to config.branchName if not present
    branch: requestBody.branch ? requestBody.branch : config.branchName,
  };

  // Extract the repoName from the repoURL
  const url = new URL(repoMetadata.repoURL);
  const split = url.pathname.split('/');
  repoMetadata.repoName = split.filter((e) => e).join('-');

  // Construct local path by combining username, repoName and branch
  repoMetadata.localPath =
    encodeURIComponent(`${repoMetadata.username}-${repoMetadata.repoName}-${repoMetadata.branch}`);

  const repoPath = `${config.reposFolder}/${repoMetadata.localPath}`;
  const repoMetadataFile = `${repoPath}/${config.repoMetadataFile}`;

  let needClone = true;
  if (fs.existsSync(repoPath)) {
    if (!fs.existsSync(repoMetadataFile)) {
      // Repo folder exists but metadata missing, will clone again
      helper.logger.info(`${repoPath} exists but metadata missing, will clone again`);
      fs.removeSync(repoPath);
    } else {
      // Check with stored repo metadata to verify user having access to the repo
      helper.checkRepoAccess(repoMetadata, repoMetadataFile,
        ['repoURL', 'username', 'repoName', 'branch', 'localPath']);

      // Will pull the update if the repo already exists
      needClone = false;
    }
  }

  // Set username/password to url
  url.username = repoMetadata.username;
  url.password = repoMetadata.password;

  try {
    if (needClone) {
      // Shallow clone repo with --depth as 1
      try {
        await GitWrapper.Git().clone(url.href, repoPath, ['-b', repoMetadata.branch, '--depth', 1]);
        helper.logger.info(`Cloned repo: ${repoPath}`);
      } catch (err) {
        // If error while clone remove the repo dir
        fs.removeSync(repoPath);
        throw err;
      }
    } else {
      // Reset the branch and pull the update
      const git = GitWrapper.Git(repoPath);
      await git.reset(['--hard', '@{u}']);
      await git.pull(url.href, repoMetadata.branch);
      helper.logger.info(`Pulled repo: ${repoPath}`);
    }
  } catch (err) {
    throw helper.convertGitError(err);
  }

  const loggedInUsersMetaFile = `${config.reposFolder}/${config.loggedInUsersMetaFile}`;
  let users = Cache.get('users');
  users = _.union(users, [repoMetadata.username]);
  Cache.set('users', users);
  fs.outputJsonSync(loggedInUsersMetaFile, users);

  // Create token payload
  const tokenPayload: any = {
    username: repoMetadata.username,
    localPath: repoMetadata.localPath,
    iat: Math.floor(Date.now() / 1000),
    userAgent: requestInfo.userAgent,
    ip: requestInfo.ip,
  };
  tokenPayload.exp = tokenPayload.iat + Math.floor(ms(config.jwtExpiresIn) / 1000);

  // Sign token and set to repo metadata
  repoMetadata.token = jwt.sign(tokenPayload, config.jwtSecret);
  repoMetadata.tokenValidUntil = new Date(tokenPayload.exp * 1000);

  // Encrypt password
  repoMetadata.password = helper.encrypt(repoMetadata.password),

    // Save repo metadata locally
    fs.outputJsonSync(repoMetadataFile, repoMetadata);

  return {
    token: repoMetadata.token,
    repoName: repoMetadata.repoName,
    validUntil: repoMetadata.tokenValidUntil,
  };
}

// The schema for the accessRepo method.
accessRepo['schema'] = {
  requestBody: Joi.object().keys({
    branch: Joi.string().optional(),
    repoURL: Joi.string().uri().required(),
    username: Joi.string().required(),
    password: Joi.string().required(),
  }).required(),
  requestInfo: Joi.object().keys({
    userAgent: Joi.string().required(),
    ip: Joi.string().ip().required(),
  }),
};

/**
 * Check file params against repo metadata from credentials.
 * @param credentials The request credentials
 * @param params The file params
 * @param fileExists Flag indicates to check file existence
 * @returns repo path, file path and full file path
 * @private
 */
const checkFileParams = (credentials, params, fileExists?: boolean) => {
  if (credentials.repoName !== params.repoName) {
    throw new errors.BadRequestError('Repo name mismatch');
  }

  const repoPath = `${config.reposFolder}/${credentials.localPath}`;
  const filePath = params.filepath ? encodeURIComponent(params.filepath) : undefined;
  const fullFilePath = `${repoPath}/${filePath}`;

  if (!_.isUndefined(fileExists)) {
    if (!fileExists && fs.existsSync(fullFilePath)) {
      throw new errors.ConflictError(`${filePath} already exists`);
    } else if (fileExists && !fs.existsSync(fullFilePath)) {
      throw new errors.NotFoundError(`${filePath} does not exist`);
    }
  }

  return [repoPath, filePath, fullFilePath];
};

/**
 * Push to repo.
 * @param repoPath The local repo path
 * @param credentials The credentials to use
 * @param commit The commit action hooker
 * @private
 */
const pushToRepo = async (repoPath: string, credentials: any, commit: (git: simpleGit.SimpleGit) => Promise<void>) => {
  // Go to repo
  const git = GitWrapper.Git(repoPath);

  // Do commit
  await commit(git);

  try {
    // Construct url with username/password from credentials
    const url = new URL(credentials.repoURL);
    url.username = credentials.username;
    url.password = helper.decrypt(credentials.password);

    // Push
    await git.push(url.href);
  } catch (err) {
    // In case push error, rollback
    await git.reset(['--hard', '@{u}']);

    throw helper.convertGitError(err);
  }
};

/**
 * Creates the file within the given location. Please note, that only YAML files are supported.
 * @param credentials Request credentials
 * @param params Request parameters
 * @param requestBody Request body
 * @param fileExists Flag indicates to check file existence
 */
async function saveFile(credentials, params, requestBody, fileExists: boolean) {
  const [repoPath, filePath, fullFilePath] = checkFileParams(credentials, params, fileExists);

  // Convert json to yaml
  const yaml = Yaml.convertJsonToYaml(requestBody.fileContent);

  // Push to repo
  await pushToRepo(repoPath, credentials, async (git: simpleGit.SimpleGit) => {
    fs.outputFileSync(fullFilePath, yaml);

    // Add file to index
    await git.add(filePath);

    // Commit
    await git.commit(
      requestBody.message, filePath,
      { '--author': `"${credentials.username} <${credentials.username}>"` });
  });
}
// The schema for the saveFile method.
saveFile['schema'] = {
  credentials: credentialsSchema,
  params: fileParamsSchema,
  requestBody: Joi.object().keys({
    fileContent: Joi.alternatives().try(Joi.object(), Joi.array()).required(),
    message: Joi.string().required(),
  }).required(),
  fileExists: Joi.boolean().required(),
};

/**
 * Delete the file within the given location from local and remote repos.
 * @param credentials Request credentials
 * @param params Request parameters
 */
async function deleteFile(credentials, params) {
  const [repoPath, filePath, fullFilePath] = checkFileParams(credentials, params, true);

  // Push to repo
  await pushToRepo(repoPath, credentials, async (git: simpleGit.SimpleGit) => {

    // Make sure file in index
    await git.add(filePath);

    // Delete file and commit
    fs.removeSync(fullFilePath);
    await git.commit(
      `Delete ${filePath}`, filePath,
      { '--author': `"${credentials.username} <${credentials.username}>"` });
  });
}
// The schema for the deleteFile method.
deleteFile['schema'] = {
  credentials: credentialsSchema,
  params: fileParamsSchema,
};

/**
 * Get the content of the file within the given location.
 * @param credentials Request credentials
 * @param params Request parameters
 */
async function getFile(credentials, params) {
  const [, , fullFilePath] = checkFileParams(credentials, params, true);

  // Convert yaml to json
  return Yaml.convertYamlToJson(fs.readFileSync(fullFilePath).toString());
}
// The schema for the getFile method.
getFile['schema'] = {
  credentials: credentialsSchema,
  params: fileParamsSchema,
};

/**
 * List the yaml files for the repo. Only yaml file in root folder are listed.
 * @param credentials Request credentials
 * @param params Request parameters
 * @returns yaml files in root folder of the repo
 */
function listFiles(credentials, params) {
  const [repoPath] = checkFileParams(credentials, params);

  const found = [];
  const files = fs.readdirSync(repoPath);
  files.forEach((filename) => {
    const stat = fs.statSync(path.resolve(repoPath, filename));
    if (stat.isDirectory()) {
      return;
    }
    const ext = path.extname(filename).toLowerCase();
    if (ext === '.yaml' || ext === '.yml') {
      found.push({
        filename,
        size: stat.size,
      });
    }
  });
  return found;
}
// The schema for the listFiles method.
listFiles['schema'] = {
  credentials: credentialsSchema,
  params: Joi.object().keys({
    repoName: Joi.string().required(),
  }).required(),
};

const service = {
  accessRepo,
  saveFile,
  deleteFile,
  getFile,
  listFiles,
};
helper.buildService(service, 'FileManagementService');

export default service;
