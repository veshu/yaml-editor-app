import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';
import { Observable } from 'rxjs';

/**
 * This service provides the methods around the file management API endpoints
 */
@Injectable({ providedIn: 'root' })
export class FileManagementService {

    /**
     * initializes the service
     * @param httpHelperService the http helper service
     */
    constructor(private httpHelperService: HttpHelperService) { }

    /**
     * access the repository and receives the security token to be used in subsequent requests
     * @param repositoryURL the repository URL
     * @param username the username
     * @param password the password
     */
    accessRepo(repositoryURL: string, username: string, password: string): Observable<any> {
        return this.httpHelperService.post('/accessRepo', { repoURL: repositoryURL, username, password });
    }

    /**
     * get files for a particular repository
     * @param repositoryName the repository name
     */
    getFiles(repository: string): Observable<any> {
        return this.httpHelperService.get(`/repos/${repository}/files`);
    }

    /*
     * get all the environments
     */
    getEnvironments(): Observable<Array<string>> {
        return this.httpHelperService.get('/environments');
    }

    /**
     * get file content of provided file path
     * @param repositoryName the repository name
     * @param filePath the file path
     */
    getFileContent(repositoryName: string, filePath: string): Observable<any> {
        return this.httpHelperService.get(`/repos/${repositoryName}/files/${filePath}`);
    }

    /**
     * Creates the file within the file path of repository
     * @param repositoryName the repository name
     * @param filePath the file name
     * @param fileContent the file content
     * @param message the commit message
     */
    createNewFile(repositoryName: string, filePath: string, fileContent: any, message: string): Observable<any> {
        const body = { message, fileContent };
        return this.httpHelperService.post(`/repos/${repositoryName}/files/${filePath}`, body);
    }

    /**
     * updates the file of the file path of repository
     * @param repositoryName the repository name
     * @param filePath the file name
     * @param fileContent the file content
     * @param message the commit message
     */
    updateFile(repositoryName: string, filePath: string, fileContent: any, message: string): Observable<any> {
        const body = { message, fileContent };
        return this.httpHelperService.put(`/repos/${repositoryName}/files/${filePath}`, body);
    }

    /**
     * deletes the file within the given location from the repository
     * @param repositoryName the repository name
     * @param filePath the file path
     */
    deleteFile(repositoryName: string, filePath: string): Observable<any> {
        return this.httpHelperService.delete(`/repos/${repositoryName}/files/${filePath}`);
    }
}
