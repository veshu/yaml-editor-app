import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { BehaviorSubject } from 'rxjs';
import { some, find, reduce, remove, forEach, map, filter, includes, get, join, keys, omit } from 'lodash';

import { PROPERTY_VALUE_TYPES } from '../../config/index';
import { TreeNode } from '../../models/tree-node';
import { MatDialog } from '@angular/material';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { Store } from '@ngrx/store';
import * as appStore from '../../store';


/**
 *  Tree with nested nodes
 */
@Component({
  selector: 'app-nested-config-view',
  templateUrl: './nested-config-view.component.html',
  styleUrls: ['./nested-config-view.component.scss']
})
export class NestedConfigViewComponent implements OnInit {
  currentAddEditProperty: TreeNode;
  currentAddEditPropertyOptions: any;
  @Input() configuration: any;
  @Input() environments: Array<string>;
  @Input() selectedEnvironments: string[] = [];

  @Output() configurationChange = new EventEmitter<any>();
  @Output() selectedNode = new EventEmitter<TreeNode>();
  @Output() addEditProperty = new EventEmitter<any>();
  @Output() cancelAddEditPropertyChange = new EventEmitter<any>();
  @Output() viewCompiledYAMLEvent = new EventEmitter<string>();

  dataSourceChange = new BehaviorSubject<TreeNode[]>([]);
  nestedTreeControl: NestedTreeControl<TreeNode>;
  nestedDataSource: MatTreeNestedDataSource<TreeNode>;

  get dataSource(): TreeNode[] { return this.dataSourceChange.value; }

  /**
   * initializes the component
   * @param dialog the material dialog instance
   * @param store the application store
   */
  constructor(private dialog: MatDialog, private store: Store<appStore.AppState>, ) {
    this.nestedTreeControl = new NestedTreeControl<TreeNode>(this._getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource();
  }

  /**
   * handle component initialization
   */
  ngOnInit() {
    this.configuration = this.configuration || { default: { $type: 'object' }, environments: { $type: 'object' } };
    forEach(this.configuration, root => {
      if (root['$type']) {
        root['$type'] = PROPERTY_VALUE_TYPES.OBJECT;
        delete root['$value'];
      }
    });
    forEach(this.configuration.environments, environment => {
      if (environment['$type']) {
        environment['$type'] = PROPERTY_VALUE_TYPES.OBJECT;
        delete environment['$value'];
      }
    });
    const dataSource = this.buildConfigTree(this.configuration, 0);
    this.dataSourceChange.next(dataSource);

    this.dataSourceChange.subscribe(data => {
      this.nestedDataSource.data = null;
      this.nestedDataSource.data = data;
    });

    this.setValueType(filter(this.nestedDataSource.data, { id: 'environments' }));
  }

  // get a node's children
  private _getChildren = (node: TreeNode) => node.children;

  // compute the value type for node based on value
  setValueType(nodes: TreeNode[]) {
    forEach(nodes, treeNode => {
      if (treeNode.key === '$comment' || treeNode.key === '$type') {
        treeNode.valueType = PROPERTY_VALUE_TYPES.STRING;
        return;
      }
      if (treeNode.level === 1) {
        delete treeNode.value;
      } else {
        const path = [treeNode.key];
        let _parentNode = treeNode.parent;
        while (_parentNode && _parentNode.level > 1) {
          path.unshift(_parentNode.key);
          _parentNode = _parentNode.parent;
        }
        path.unshift('default');
        const rootDefaultNode = find(this.nestedDataSource.data, { id: 'default' });
        const defaultNode = find(this.nestedTreeControl.getDescendants(rootDefaultNode), { id: join(path, '.') });
        if (defaultNode) {
          treeNode.valueType = defaultNode.valueType;
        }
      }
      if (treeNode.valueType === PROPERTY_VALUE_TYPES.OBJECT) {
        treeNode.value = undefined;
      }
      this.setValueType(treeNode.children);
    });
  }

  /**
   * Build the config structure tree. The `value` is the Json object, or a sub-tree of a Json object.
   * The return value is the list of `TreeNode`.
   */
  buildConfigTree(obj: object, level: number, parentNode?: TreeNode, type: string = 'object'): TreeNode[] {
    return Object.keys(obj).reduce<TreeNode[]>((accumulator, key) => {
      const value = obj[key];
      const treeNode = new TreeNode();
      treeNode.key = key;
      treeNode.id = parentNode ? `${parentNode.id}.${key}` : key;
      if (level !== 0) {
        treeNode.parent = parentNode;
      }
      treeNode.valueType = value['$type'];
      if (value != null) {
        if (typeof value === PROPERTY_VALUE_TYPES.OBJECT) {
          treeNode.children = this.buildConfigTree(value, level + 1, treeNode, value.$type);
        } else {
          if (key === '$comment' || key === '$value') {
            treeNode.value = value;
          } else {
            treeNode.value = key === '$type' ? type : value.$value;
          }
        }
      }
      if (treeNode.valueType === PROPERTY_VALUE_TYPES.OBJECT) {
        treeNode.value = undefined;
        treeNode.children = treeNode.children ? treeNode.children : [];
      }
      treeNode.level = level;
      treeNode.jsonValue = this.buildObjectTree([treeNode]);
      treeNode.isLeaf = true;

      return accumulator.concat(treeNode);
    }, []);
  }


  /*
   * reverse of buildConfigTree
   * this function builds the config object from the nestedDataSource
   */
  buildObjectTree(treeArray: TreeNode[]) {
    return reduce(treeArray, (accumulator, node: TreeNode) => {
      accumulator[node.key] = node.value !== undefined ? node.value : this.buildObjectTree(node.children);
      return accumulator;
    }, {});
  }


  /*
   * when condition in mat-nested-tree-node
   * which returns true if a node has children
   */
  hasNestedChild = (_: number, node: TreeNode) => node.value === undefined || node.valueType === PROPERTY_VALUE_TYPES.OBJECT;


  /*
   * to check if the children of the given node has $value as a key
   * if true then this node is a simple node to which more properties cannot be added
   */
  isSimpleNestedNode(node: TreeNode): boolean {
    return node.valueType !== PROPERTY_VALUE_TYPES.OBJECT;
  }

  /**
   * gets the value of node
   * @param node node to get value from
   */
  getNodeValue(node: TreeNode): string {
    const value = node.value || find(node.children, { key: '$value' }).value;
    return value;
  }

  /*
   * insert new config property
   */
  insertItem(parentNode, newNode: TreeNode) {
    if (parentNode.key) {
      parentNode.children = parentNode.children || [];
      parentNode.children.push(newNode);
    } else {
      // if its the root node
      parentNode.push(newNode);
    }
    this.refreshTree();
  }

  /*
   * add new property
   */
  openAddPropertyDialog(node: TreeNode) {
    const isDefaultEnvironmentNode = node.isDefaultEnvironmentNode();
    this.currentAddEditProperty = node;
    this.currentAddEditPropertyOptions = {
      editMode: false,
      isDefaultEnvironmentNode,
      ...this.getEnvOptions(node, false),
      level: node.level,
      node
    };
    this.addEditProperty.emit(this.currentAddEditPropertyOptions);
  }

  /**
   * prepare the dropdown options based node and mode
   */
  getEnvOptions(node: TreeNode, editMode: boolean) {
    const envOptions = {
      keyOptions: []
    };
    if (node.isDefaultEnvironmentNode()) {
      return envOptions;
    }
    if (node.level === 0 && editMode) {
      envOptions.keyOptions.push({ key: node.key, type: PROPERTY_VALUE_TYPES.OBJECT });
    } else if (((node.level === 1 && editMode) || (node.level === 0 && !editMode))) {
      envOptions.keyOptions = map(this.environments, environment => {
        return { key: environment, type: PROPERTY_VALUE_TYPES.OBJECT };
      });
      if (!editMode) {
        envOptions.keyOptions = filter(envOptions.keyOptions, option => !includes(keys(this.configuration.environments), option.key));
      }
    } else {
      const keyHierarchy = node.level > 1 ? [node.key] : [];
      let parentNode = node.parent;
      while (parentNode && parentNode.level > 1) {
        keyHierarchy.unshift(parentNode.key);
        parentNode = parentNode.parent;
      }

      parentNode = find(this.nestedDataSource.data, { key: 'default' });
      for (let i = 0; i < keyHierarchy.length; i++) {
        parentNode = find(parentNode.children, { key: keyHierarchy[i] });
      }
      parentNode = editMode ? parentNode.parent : parentNode;
      if (parentNode) {
        const childNodes = filter(parentNode.children, child => child.key !== '$comment');
        envOptions.keyOptions = map(childNodes, childNode => {
          return {
            key: childNode.key,
            type: childNode.valueType
          };
        });
      }
      if (!editMode) {
        envOptions.keyOptions = filter(envOptions.keyOptions,
          option => !includes(keys(get(this.configuration, node.id)), option.key));
      }
    }
    return envOptions;
  }

  /*
   * edit existing property
   */
  openEditPropertyDialog(node: TreeNode) {
    // construct the property object
    const propertyObject = node.toConfigProperty();
    this.currentAddEditProperty = node;
    this.currentAddEditPropertyOptions = {
      editMode: true,
      isDefaultEnvironmentNode: node.isDefaultEnvironmentNode(),
      ...propertyObject,
      level: node.level,
      ...this.getEnvOptions(node, true),
      node
    };
    this.addEditProperty.emit(this.currentAddEditPropertyOptions);
  }

  /**
   * Refresh the tree
   */
  refreshTree() {
    this.dataSourceChange.next(this.dataSource);
    const _data = this.nestedDataSource.data;
    this.nestedDataSource.data = null;
    this.nestedDataSource.data = _data;
    this.configurationChange.emit(this.buildObjectTree(this.nestedDataSource.data));
  }

  /**
   * saves the node
   * @param node the added/edited node
   */
  saveAddEditProperty(node: TreeNode) {
    if (this.currentAddEditPropertyOptions.editMode === true) {
      if (!this.currentAddEditProperty.isDefaultEnvironmentNode()) { // for other environment value types should be changed
        node.valueType = this.currentAddEditProperty.valueType;
      }
      if (this.currentAddEditProperty.key !== node.key || this.currentAddEditProperty.valueType !== node.valueType) {
        this.deleteEnvironmentProperties(this.currentAddEditProperty);
      }
      this.currentAddEditProperty.key = node.key;
      this.currentAddEditProperty.value = node.value;
      this.currentAddEditProperty.valueType = node.valueType;
      if (node.valueType === PROPERTY_VALUE_TYPES.OBJECT) {
        // remove value field if property is changed to object
        this.currentAddEditProperty.children = filter(this.currentAddEditProperty.children, children => {
          return children && children.key !== '$value';
        });
        // if node is object then only comment should be updated
        const comment = find(node.children, { 'key': '$comment' });
        // check if there was existing comment then replace value else add the comment as children
        if (!find(this.currentAddEditProperty.children, { 'key': '$comment' })) {
          this.currentAddEditProperty.children.unshift(comment);
        } else {
          this.currentAddEditProperty.children = map(this.currentAddEditProperty.children, children => {
            if (children.key === '$comment') {
              children.value = comment.value;
            }
            return children;
          });
        }
        this.currentAddEditProperty.children = filter(this.currentAddEditProperty.children, children => {
          return children !== undefined;
        });
      } else {
        this.currentAddEditProperty.children = node.children;
      }
      this.updateJsonValue(this.currentAddEditProperty);
    } else {
      node.level = this.currentAddEditProperty.level + 1;
      node.parent = this.currentAddEditProperty;
      node.id = `${this.currentAddEditProperty.id}.${node.key}`;
      this.insertItem(this.currentAddEditProperty, node);
      this.nestedTreeControl.expand(this.currentAddEditProperty);
      this.updateJsonValue(node);
    }
    this.refreshTree();
    this.cancelAddEditProperty();
  }

  /**
   * updates the json value of node and its all parent
   * @param node the node to update
   */
  updateJsonValue(node: TreeNode) {
    node.jsonValue = this.buildObjectTree([node]);
    let parent = node.parent;
    while (parent) {
      parent.jsonValue = this.buildObjectTree([parent]);
      parent = parent.parent;
    }
  }

  /**
   * cancels the add/edit property action
   */
  cancelAddEditProperty() {
    this.cancelAddEditPropertyChange.emit();
  }

  /**
   * deletes the property from configuration
   * @param node node to delete
   */
  deleteProperty(node: TreeNode) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        confirmationText: 'Are you sure you want to delete this property?'
      }
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response) {
        this.cancelAddEditProperty();
        const nodeArray = node.parent ? node.parent.children : this.nestedDataSource.data;
        remove(nodeArray, item => item.key === node.key);
        this.updateJsonValue(node.parent);
        // if deleted node is from default tree delete respective properties from other environments
        if (node.isDefaultEnvironmentNode()) {
          this.deleteEnvironmentProperties(node);
        }
        this.refreshTree();
      }
    });
  }

  /**
   * shows the detail of node in right side
   * @param node node to show as selected
   */
  showDetail(node: TreeNode) {
    node.isLeaf = this.isSimpleNestedNode(node) || !node.children;
    this.selectedNode.emit(node);
  }

  /**
   * view compiled YAML action handler
   * @param environment the environment to compile
   */
  viewCompiledYAML(environment: string) {
    this.viewCompiledYAMLEvent.emit(environment);
  }

  /**
   * deletes the corresponding nodes from other environments
   * @param node node which is deleted
   */
  private deleteEnvironmentProperties(node: TreeNode) {
    const selectedEnvironments = keys(this.configuration.environments);
    for (let i = 0; i < selectedEnvironments.length; i++) {
      // replace first default
      const path = node.id.replace('default', `environments.${selectedEnvironments[i]}`);
      const parentEnvironmentNode = find(this.nestedDataSource.data, { id: 'environments' });
      const environmentNode = find(this.nestedTreeControl.getDescendants(parentEnvironmentNode), { id: path });
      if (environmentNode) {
        remove(environmentNode.parent.children, item => item.key === node.key);
        this.updateJsonValue(environmentNode.parent);
      }
    }
  }
}
