import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { values, find } from 'lodash';

import { PROPERTY_VALUE_TYPES } from '../../config/index';
import { ConfigProperty } from '../../models/config-property';
import { UtilService } from '../../services/util.service';
import { TreeNode } from '../../models/tree-node';

/*
  add or edit new property in the environment configuration
  if its default environment then the property key is a text input
  and if its a custom environment then its a select dropdown
 */
@Component({
  selector: 'app-add-edit-property-dialog',
  templateUrl: './add-edit-property-dialog.component.html',
  styleUrls: ['./add-edit-property-dialog.component.scss']
})
export class AddEditPropertyDialogComponent implements OnChanges {
  @Input() data: any;
  @Output() saveProperty = new EventEmitter<TreeNode>();
  @Output() cancel = new EventEmitter<any>();
  formDirty = false;
  valueTypeOptions = values(PROPERTY_VALUE_TYPES);
  key: FormControl;
  valueType: FormControl;
  value: FormControl;
  comment: FormControl;

  constructor(private utilService: UtilService) { }

  ngOnChanges() {
    this.key = new FormControl('', [Validators.required]);
    this.valueType = new FormControl('', [Validators.required]);
    this.value = new FormControl('', [Validators.required]);
    this.comment = new FormControl('');
    const { key, value, valueType, comment, isDefaultEnvironmentNode, level } = this.data;
    if (this.data.editMode) {
      this.key.setValue(key);
      this.valueType.setValue(valueType);
      // any other environment the key and valueType should be disabled to edit
      // only value and comment can be edited
      if (!isDefaultEnvironmentNode || level === 0) {
        this.key.disable();
        this.valueType.disable();
      }
      if (valueType === PROPERTY_VALUE_TYPES.BOOLEAN) {
        this.value.setValue(value !== undefined ? value.toString() : '');
      } else {
        this.value.setValue(value ? value : '');
      }

      this.comment.setValue(comment ? comment : '');
    } else {

      if (!isDefaultEnvironmentNode) {
        this.valueType.disable();
      }
    }
  }


  /*
    set value type in other environments when key is selected
   */
  setValueTypeOption(value: string) {
    this.valueType.setValue(find(this.data.keyOptions, { key: value }).type);
  }


  /*
    submit the property form with new/updated property values
   */
  onSubmit() {
    this.formDirty = true;

    // check form validity
    if (
      (this.key.disabled ? true : this.key.valid) &&
      (this.valueType.disabled ? true : this.valueType.valid) &&
      (this.valueType.value === 'object' ? true : this.value.valid)
    ) {
      const property = new ConfigProperty();
      property.key = this.key.value;
      property.value = this.value.value;
      property.valueType = this.valueType.value;
      property.comment = this.comment.value;
      if (property.valueType === PROPERTY_VALUE_TYPES.OBJECT) {
        property.value = undefined;
      }

      // omit empty values and send the response and close the dialog
      this.saveProperty.emit(this.utilService.convertToTreeNode(property));
    }
  }

  onCancel() {
    this.cancel.emit();
  }

  isChecked(value, flag) {
    return value === flag;
  }
}
