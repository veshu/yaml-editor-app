import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import * as appStore from '../../store';
import { FileManagementService } from '../../services/file-management.service';
import { NestedConfigViewComponent } from '../../components/nested-config-view/nested-config-view.component';
import { UtilService } from '../../services/util.service';
import { TreeNode } from '../../models/tree-node';
import { ConfirmationDialogComponent } from '../../components/confirmation-dialog/confirmation-dialog.component';
import { map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import {
  PageLoad, ViewCompiledYAML, OpenAddEditProperty,
  CancelAddEditProperty, SaveAddEditProperty, ConfigurationChange,
  NodeSelected,
  SaveFile
} from '../../store/actions/config-file-add-edit.actions';
import { Configuration } from '../../models/config-file';
import { Observable } from 'rxjs';

/*
  Configurations editor page
  for both editing existing files and adding new ones
 */
@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {
  isPageDirty$ = this.store.pipe(select(appStore.getIsPageDirty));
  isPageDirty = false;
  filename = new FormControl('', [Validators.required]);

  environments: Observable<string[]> = this.store.pipe(select(appStore.getEnvironments));

  inEditMode: Observable<boolean> = this.store.pipe(select(appStore.getMode));
  // only if its in edit mode
  filePath: Observable<string> = this.store.pipe(select(appStore.getFilePath));
  configuration: Observable<Configuration> = this.store.pipe(select(appStore.getConfiguration));
  selectedEnvironments = this.store.pipe(select(appStore.getSelectedEnvironments));
  selectedNode = this.store.pipe(select(appStore.getSelectedNode));
  showAsCode = this.store.pipe(select(appStore.getShowAsCode));
  showAsCompiledYAMLEnvironment = this.store.pipe(select(appStore.getShowAsCompiledYAMLEnvironment));
  previewCode = this.store.pipe(select(appStore.getPreviewCode));
  currentAddEditProperty = this.store.pipe(select(appStore.getCurrentAddEditProperty));
  selectedConfigProperty = this.store.pipe(select(appStore.getSelectedConfigProperty));
  isSavingData = this.store.pipe(select(appStore.getIsSaving));
  isEditMode = false;

  @ViewChild('nestedConfig') nestedConfig: NestedConfigViewComponent;
  @HostListener('window:beforeunload', ['$event'])
  onLeavePage($event: any) {
    if (this.isPageDirty) {
      $event.returnValue = true;
    }
  }

  constructor(
    private route: ActivatedRoute,
    private store: Store<appStore.AppState>,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    // get file content if its in edit mode
    const routeSnapshot = this.route.snapshot;
    this.isEditMode = routeSnapshot.data.inEditMode;
    const filePath = this.isEditMode ? routeSnapshot.paramMap.get('filePath') : null;
    this.store.dispatch(new PageLoad({ inEditMode: this.isEditMode, filePath }));
    if (filePath) {
      this.filename.setValue(filePath);
      this.filename.disable();
    } else {
      this.filename.setValue('');
    }
    this.isPageDirty$.subscribe(res => {
      this.isPageDirty = res;
    });
  }

  // checks if component can be deactivated
  canDeactivate() {
    if (this.isPageDirty) {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data: {
          confirmationText: 'There may be unsaved changes. Are you sure you want to navigate?'
        }
      });
      return dialogRef.afterClosed().pipe(map(response => response));
    }
    return true;
  }

  // handles the configuration change request
  onConfigChange(configuration) {
    this.isPageDirty = true;
    this.store.dispatch(new ConfigurationChange({ configuration, isPageDirty: this.isPageDirty }));
  }

  /*
   * save the config if it has been changed
   */
  saveConfig() {
    if (this.filename.invalid && !this.isEditMode) {
      this.filename.markAsTouched();
      return;
    }
    this.filename.markAsDirty();
    const filePath = this.filename.value.match(/\.[y|Y][a|A]?[m|M][l|L]$/) ? this.filename.value : this.filename.value + '.yaml';
    this.store.dispatch(new SaveFile({ filePath }));
  }

  // handles the node selected request to show the detail
  onNodeSelected(node: TreeNode) {
    this.store.dispatch(new NodeSelected({ node }));
  }

  // handles the open add/edit property request
  onAddEditProperty(options) {
    this.store.dispatch(new OpenAddEditProperty({ options }));
  }

  // handles the cancel add/edit property request
  onCancelAddEditProperty() {
    this.store.dispatch(new CancelAddEditProperty());
  }

  // handles the save add/edit property request
  onSaveAddEditProperty(node: TreeNode) {
    this.nestedConfig.saveAddEditProperty(node);
    this.store.dispatch(new SaveAddEditProperty({ node }));
  }

  // handles the compiled YAML view request
  showCompiledYAML(environment: string) {
    this.store.dispatch(new ViewCompiledYAML({ environment }));
  }

}
