import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfirmationDialogComponent } from '../../components/confirmation-dialog/confirmation-dialog.component';
import { Store, select } from '@ngrx/store';
import * as appStore from '../../store';
import { map } from 'rxjs/operators';
import { LoadFiles, DeleteFile } from '../../store/actions/dashboard.actions';
import { ConfigFile } from '../../models/config-file';
import { Observable } from 'rxjs';

/*
  Dashboard page
 */
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  repositoryName: Observable<string> = this.store.pipe(select(appStore.getRepositoryName));
  files: Observable<ConfigFile[]> = this.store.pipe(select(appStore.getConfigFiles));
  isDeleting = this.store.pipe(select(appStore.getDashboardFileDeleting));

  displayedColumns: string[] = ['id', 'filename', 'actions'];

  /**
   * initializes the component
   * @param store the application store
   * @param dialog the material dialog instance
   */
  constructor(
    private store: Store<appStore.AppState>,
    private dialog: MatDialog
  ) { }

  /**
   * handle component initialization
   */
  ngOnInit() {
    this.store.dispatch(new LoadFiles());
  }

  /**
   * deletes the file
   * @param filePath the file path
   */
  deleteFile(filePath: string) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        confirmationText: 'Are you sure you want to delete this file?'
      }
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response) {
        this.store.dispatch(new DeleteFile({ filePath }));
      }
    });
  }
}
