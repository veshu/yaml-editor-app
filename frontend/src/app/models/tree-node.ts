import { ConfigProperty } from './config-property';
import { PROPERTY_VALUE_TYPES } from '../config';
import { find } from 'lodash';

/**
 * Json node data with nested structure. Each node has a key and a value or a list of children
 */
export class TreeNode {
    id: string;
    children: TreeNode[];
    parent: TreeNode;
    key: string;
    value: any;
    level: number;
    jsonValue: any;
    isLeaf: boolean;
    valueType: string;

    toConfigProperty(): ConfigProperty {
        const property = new ConfigProperty();
        property.key = this.key;
        const valueNode = find(this.children, { key: '$value' });
        property.value = valueNode ? valueNode.value : this.value;
        property.valueType = property.value ? typeof property.value : PROPERTY_VALUE_TYPES.OBJECT;
        property.valueType = this.valueType ? this.valueType : property.valueType;
        const commentNode = find(this.children, { key: '$comment' });
        property.comment = commentNode ? commentNode.value : null;
        property.isRoot = this.level === 0;
        property.isDefaultEnvironmentNode = this.isDefaultEnvironmentNode();

        return property;
    }

    isDefaultEnvironmentNode() {
        if (this.level === 0 && this.key === 'default') {
            return true;
        }
        if (this.level === 0 && this.key === 'environments') {
            return false;
        }
        const parentNode = this.getTopParent();
        return parentNode && parentNode.level === 0 && parentNode.key === 'default';
    }

    getTopParent() {
        let parentNode = this.parent;
        while (parentNode && parentNode.parent) {
            parentNode = parentNode.parent;
        }
        return parentNode;
    }
}
