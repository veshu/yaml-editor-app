export interface Authenticate {
    username: string;
    password: string;
    repositoryURL: string;
}

export interface LoginResult {
    token: string;
    repoName: string;
    validUntil: string;
}

export interface User extends LoginResult {
    username: string;
}
