export interface ConfigFile {
    filename: string;
    size: number;
}

export interface Configuration {
    default: object;
    environments: object;
}
