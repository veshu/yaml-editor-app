/**
 * Class that describes the config property for add/edit
 */
export class ConfigProperty {
    key: string;
    value: any;
    valueType: string;
    comment?: string;
    isRoot: boolean; // represents if node is root i.e. either default or environment
    isDefaultEnvironmentNode: boolean; // represents if property is default or not, false means it is environments root node
}
