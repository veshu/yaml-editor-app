import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HighlightModule } from 'ngx-highlightjs';

// angular material components
import { MaterialComponentsModule } from './material-components/material-components.module';

// app routing module
import { AppRoutingModule } from './app-routing.module';

// services
import { UtilService } from './services/util.service';
import { HttpHelperService } from './services/http-helper.service';
import { AuthGuardService } from './services/auth-guard.service';
import { FileManagementService } from './services/file-management.service';

// main app component
import { AppComponent } from './app.component';

// pages
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { EditorComponent } from './pages/editor/editor.component';

// components
import { LayoutComponent } from './components/layout/layout.component';
import { MainHeaderComponent } from './components/main-header/main-header.component';
import { LoaderComponent } from './components/loader/loader.component';
import { AddEditPropertyDialogComponent } from './components/add-edit-property-dialog/add-edit-property-dialog.component';
import { NestedConfigViewComponent } from './components/nested-config-view/nested-config-view.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { AlertDialogComponent } from './components/alert-dialog/alert-dialog.component';
import { CanDeactivateGuard } from './services/can-deactivate-guard.service';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './store/affects/app.effects';
import { DashboardEffects } from './store/affects/dashboard.effects';
import { AuthEffects } from './store/affects/auth.affects';
import { ConfigFileAddEditEffects } from './store/affects/config-file-add-edit.effects';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    LayoutComponent,
    MainHeaderComponent,
    EditorComponent,
    LoaderComponent,
    AddEditPropertyDialogComponent,
    NestedConfigViewComponent,
    ConfirmationDialogComponent,
    AlertDialogComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    MaterialComponentsModule,
    HighlightModule.forRoot({ theme: 'default' }),
    StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([AuthEffects, AppEffects, DashboardEffects, ConfigFileAddEditEffects])
  ],
  entryComponents: [
    AddEditPropertyDialogComponent,
    ConfirmationDialogComponent,
    AlertDialogComponent
  ],
  providers: [
    UtilService,
    HttpHelperService,
    AuthGuardService,
    FileManagementService,
    CanDeactivateGuard,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
