import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatAutocompleteModule,
  MatCardModule,
  MatButtonModule,
  MatInputModule,
  MatTableModule,
  MatSelectModule,
  MatDialogModule,
  MatDividerModule,
  MatTabsModule,
  MatTreeModule,
  MatMenuModule,
  MatRadioModule,
  MatTooltipModule,
} from '@angular/material';


@NgModule({
  imports: [
    MatAutocompleteModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatTableModule,
    MatSelectModule,
    MatDialogModule,
    MatDividerModule,
    MatTabsModule,
    MatTreeModule,
    MatMenuModule,
    MatRadioModule,
    MatTooltipModule
  ],
  exports: [
    MatAutocompleteModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatTableModule,
    MatSelectModule,
    MatDialogModule,
    MatDividerModule,
    MatTabsModule,
    MatTreeModule,
    MatMenuModule,
    MatRadioModule,
    MatTooltipModule
  ]
})

export class MaterialComponentsModule { }
