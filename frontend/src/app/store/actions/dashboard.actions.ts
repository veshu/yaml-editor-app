import { Action } from '@ngrx/store';
import { User } from '../../models/auth';
import { ConfigFile } from '../../models/config-file';

export enum DashboardActionTypes {
  LoadFiles = '[Dashboard] Load Files',
  LoadFilesSuccess = '[Dashboard] Load Files Success',
  LoadFilesFailure = '[Dashboard] Load Files Failure',
  EditFile = '[Dashboard] Edit File',
  DeleteFile = '[Dashboard] Delete File',
  DeleteFileFailure = '[Dashboard] Delete File Failure',
  DeleteFileSuccess = '[Dashboard] Delete File Success',
  ADDFile = '[Dashboard] Add File',
}

export class LoadFiles implements Action {
  readonly type = DashboardActionTypes.LoadFiles;
}

export class LoadFilesSuccess implements Action {
  readonly type = DashboardActionTypes.LoadFilesSuccess;

  constructor(public payload: ConfigFile[]) { }
}

export class LoadFilesFailure implements Action {
  readonly type = DashboardActionTypes.LoadFilesFailure;

  constructor(public payload: any) { }
}

export class DeleteFile implements Action {
  readonly type = DashboardActionTypes.DeleteFile;

  constructor(public payload: { filePath: string }) { }
}

export class DeleteFileSuccess implements Action {
  readonly type = DashboardActionTypes.DeleteFileSuccess;
  constructor(public payload: { filePath: string }) { }
}

export class DeleteFileFailure implements Action {
  readonly type = DashboardActionTypes.DeleteFileFailure;

  constructor(public payload: any) { }
}


export type DashboardActionsUnion =
  | LoadFiles
  | LoadFilesSuccess
  | LoadFilesFailure
  | DeleteFile
  | DeleteFileSuccess
  | DeleteFileFailure;
