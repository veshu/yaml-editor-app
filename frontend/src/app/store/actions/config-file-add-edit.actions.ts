import { Action } from '@ngrx/store';
import { User } from '../../models/auth';
import { Configuration } from '../../models/config-file';
import { TreeNode } from '../../models/tree-node';
import { ConfigProperty } from '../../models/config-property';

export enum ConfigFileAddEditActionTypes {
    PageLoad = '[Config File Add Edit] Page Load',
    PageLoadSuccess = '[Config File Add Edit] Page Load Success',
    PageLoadFailure = '[Config File Add Edit] Page Load Failure',
    GetFileContent = '[Config File Add Edit] Get File Content',
    GetFileContentSuccess = '[Config File Add Edit] Get File Content Success',
    GetFileContentFailure = '[Config File Add Edit] Get File Content Failure',
    ViewCompiledYAML = '[Config File Add Edit] View Compiled YAML',
    ViewCompiledYAMLSuccess = '[Config File Add Edit] View Compiled YAML Success',
    SaveFile = '[Config File Add Edit] Save File',
    SaveFileSuccess = '[Config File Add Edit] Save File Success',
    SaveFileFailure = '[Config File Add Edit] Save File Failure',
    OpenAddEditProperty = '[Config File Add Edit] Open Add Edit Property',
    CancelAddEditProperty = '[Config File Add Edit] Cancel Add Edit Property',
    SaveAddEditProperty = '[Config File Add Edit] Save Add Edit Property',
    NodeSelected = '[Config File Add Edit] Node Selected',
    NodeSelectedSuccess = '[Config File Add Edit] Node Selected Success',
    ConfigurationChange = '[Config File Add Edit] Configuration Change',
}

export class PageLoad implements Action {
    readonly type = ConfigFileAddEditActionTypes.PageLoad;

    constructor(public payload: { inEditMode: boolean, filePath: string }) { }
}

export class PageLoadSuccess implements Action {
    readonly type = ConfigFileAddEditActionTypes.PageLoadSuccess;

    constructor(public payload: { environments: string[], filePath: string }) { }
}

export class PageLoadFailure implements Action {
    readonly type = ConfigFileAddEditActionTypes.PageLoadFailure;
    constructor(public payload: any) { }
}

export class GetFileContent implements Action {
    readonly type = ConfigFileAddEditActionTypes.GetFileContent;
}

export class ConfigurationChange implements Action {
    readonly type = ConfigFileAddEditActionTypes.ConfigurationChange;
    constructor(public payload: { configuration: any, isPageDirty: boolean }) { }
}

export class GetFileContentFailure implements Action {
    readonly type = ConfigFileAddEditActionTypes.GetFileContentFailure;
    constructor(public payload: any) { }
}

export class ViewCompiledYAML implements Action {
    readonly type = ConfigFileAddEditActionTypes.ViewCompiledYAML;
    constructor(public payload: { environment: string }) { }
}

export class ViewCompiledYAMLSuccess implements Action {
    readonly type = ConfigFileAddEditActionTypes.ViewCompiledYAMLSuccess;
    constructor(public payload: { compiledYAML: string }) { }
}

export class SaveFile implements Action {
    readonly type = ConfigFileAddEditActionTypes.SaveFile;
    constructor(public payload: { filePath: string }) { }
}

export class SaveFileSuccess implements Action {
    readonly type = ConfigFileAddEditActionTypes.SaveFileSuccess;

    constructor(public payload: { inEditMode: boolean }) { }
}

export class SaveFileFailure implements Action {
    readonly type = ConfigFileAddEditActionTypes.SaveFileFailure;
    constructor(public payload: any) { }
}

export class OpenAddEditProperty implements Action {
    readonly type = ConfigFileAddEditActionTypes.OpenAddEditProperty;
    constructor(public payload: { options: any }) { }
}

export class CancelAddEditProperty implements Action {
    readonly type = ConfigFileAddEditActionTypes.CancelAddEditProperty;
}

export class SaveAddEditProperty implements Action {
    readonly type = ConfigFileAddEditActionTypes.SaveAddEditProperty;
    constructor(public payload: { node: TreeNode }) { }
}

export class NodeSelected implements Action {
    readonly type = ConfigFileAddEditActionTypes.NodeSelected;
    constructor(public payload: { node: TreeNode }) { }
}

export class NodeSelectedSuccess implements Action {
    readonly type = ConfigFileAddEditActionTypes.NodeSelectedSuccess;
    constructor(public payload: { compiledYAML: string, configProperty: ConfigProperty }) { }
}

export type ConfigFileAddEditActionsUnion =
    | PageLoad
    | PageLoadSuccess
    | PageLoadFailure
    | GetFileContent
    | GetFileContentFailure
    | ViewCompiledYAML
    | ViewCompiledYAMLSuccess
    | SaveFile
    | SaveFileSuccess
    | SaveFileFailure
    | OpenAddEditProperty
    | CancelAddEditProperty
    | SaveAddEditProperty
    | NodeSelected
    | NodeSelectedSuccess
    | ConfigurationChange;
