import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';
import { environment } from '../../environments/environment';
import * as fromAuth from './reducers/auth.reducers';
import * as fromDashboard from './reducers/dashboard.reducer';
import * as fromEditor from './reducers/config-file-add-edit.reducer';

export interface AppState {
  auth: fromAuth.State;
  dashboard: fromDashboard.State;
  editor: fromEditor.State;
}

export const reducers: ActionReducerMap<AppState> = {
  auth: fromAuth.reducer,
  dashboard: fromDashboard.reducer,
  editor: fromEditor.reducer,
};

// console.log all actions
export function logger(reducer: ActionReducer<AppState>): ActionReducer<AppState> {
  return function (state: AppState, action: any): AppState {
    console.log('state', state); // for logging purpose
    console.log('action', action); // for logging purpose

    return reducer(state, action);
  };
}
export function localStorageSyncReducer(reducer: ActionReducer<AppState>): ActionReducer<AppState> {
  return localStorageSync({ keys: [{ 'auth': ['currentUser', 'loggedIn'] }], rehydrate: true })(reducer);
}

export const metaReducers: MetaReducer<AppState>[] = !environment.production ?
  [logger, localStorageSyncReducer] :
  [localStorageSyncReducer];

// dashboard related selectors
export const authState = createFeatureSelector<AppState, fromAuth.State>('auth');

export const getLoggedIn = createSelector(authState, fromAuth.getLoggedIn);
export const getFormProcessing = createSelector(authState, fromAuth.getFormProcessing);
export const getCurrentUser = createSelector(authState, fromAuth.getLoggedInUser);
export const getRedirectUrl = createSelector(authState, fromAuth.getRedirectUrl);
export const getLoginError = createSelector(authState, fromAuth.getError);
export const getRepositoryName = createSelector(authState, fromAuth.getRepositoryName);

// dashboard related selectors
export const dashboardState = createFeatureSelector<AppState, fromDashboard.State>('dashboard');

export const getConfigFiles = createSelector(dashboardState, fromDashboard.getFiles);
export const getDashboardError = createSelector(dashboardState, fromDashboard.getError);
export const getDashboardFileDeleting = createSelector(dashboardState, fromDashboard.isDeletingFile);

// config file add/edit selectors

export const editorState = createFeatureSelector<AppState, fromEditor.State>('editor');

export const getConfiguration = createSelector(editorState, fromEditor.getConfiguration);
export const getEnvironments = createSelector(editorState, fromEditor.getEnvironments);
export const getIsSaving = createSelector(editorState, fromEditor.isSaving);
export const getEditorError = createSelector(editorState, fromEditor.getError);
export const getMode = createSelector(editorState, fromEditor.getMode);
export const getFilePath = createSelector(editorState, fromEditor.getFilePath);
export const getEditorState = createSelector(editorState, fromEditor.getState);
export const getShowAsCode = createSelector(editorState, fromEditor.getShowAsCode);
export const getPreviewCode = createSelector(editorState, fromEditor.getPreviewCode);
export const getShowAsCompiledYAMLEnvironment = createSelector(editorState, fromEditor.getShowAsCompiledYAMLEnvironment);
export const getSelectedNode = createSelector(editorState, fromEditor.getSelectedNode);
export const getCurrentAddEditProperty = createSelector(editorState, fromEditor.getCurrentAddEditProperty);
export const getSelectedConfigProperty = createSelector(editorState, fromEditor.getSelectedConfigProperty);
export const getSelectedEnvironments = createSelector(editorState, fromEditor.getSelectedEnvironments);
export const getIsPageDirty = createSelector(editorState, fromEditor.getIsPageDirty);


// all app state
export const getAppState = (state: AppState) => state;
