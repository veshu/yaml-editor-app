import { Configuration } from '../../models/config-file';
import { ConfigFileAddEditActionTypes, ConfigFileAddEditActionsUnion } from '../actions/config-file-add-edit.actions';
import * as _ from 'lodash';
import { TreeNode } from '../../models/tree-node';
import { ConfigProperty } from '../../models/config-property';
import { PROPERTY_VALUE_TYPES } from '../../config';

export interface State {
    error: any;
    isSaving: boolean;
    environments: Array<string>;
    filePath: string;
    inEditMode: boolean;
    configuration: Configuration;
    showAsCode: boolean;
    previewCode: string;
    showAsCompiledYAMLEnvironment: string;
    selectedNode: TreeNode;
    currentAddEditProperty: any;
    selectedConfigProperty: ConfigProperty;
    selectedEnvironment: string;
    selectedEnvironments: string[];
    editingProperty: TreeNode;
    isPageDirty: boolean;
}

export const initialState: State = {
    error: null,
    isSaving: false,
    filePath: null,
    environments: [],
    inEditMode: false,
    configuration: null,
    showAsCode: false,
    previewCode: null,
    showAsCompiledYAMLEnvironment: null,
    selectedNode: null,
    currentAddEditProperty: null,
    selectedConfigProperty: null,
    selectedEnvironment: null,
    selectedEnvironments: [],
    editingProperty: null,
    isPageDirty: false,

};

const cancelRightPanel = {
    isSaving: false,
    showAsCode: false,
    previewCode: null,
    showAsCompiledYAMLEnvironment: null,
    selectedNode: null,
    selectedConfigProperty: null,
    currentAddEditProperty: null,
};

export function reducer(state = initialState, action: ConfigFileAddEditActionsUnion): State {
    switch (action.type) {
        case ConfigFileAddEditActionTypes.PageLoad: {
            let defaultConfig;
            if (!action.payload.inEditMode) {
                defaultConfig = {
                    default: {
                        $type: 'object'
                    },
                    environments: {
                        $type: 'object'
                    }
                };
            }
            return {
                ...initialState,
                isSaving: false,
                isPageDirty: false,
                inEditMode: action.payload.inEditMode,
                filePath: action.payload.filePath,
                configuration: action.payload.inEditMode ? defaultConfig : null,
            };
        }

        case ConfigFileAddEditActionTypes.PageLoadSuccess: {
            return {
                ...state,
                environments: action.payload.environments,
            };
        }

        case ConfigFileAddEditActionTypes.ConfigurationChange: {
            return {
                ...state,
                configuration: {
                    default: action.payload.configuration.default || { $type: 'object' },
                    environments: action.payload.configuration.environments || { $type: 'object' }
                },
                selectedEnvironments: _.chain(action.payload.configuration.environments)
                    .keys().intersection(state.environments).value(),
                isPageDirty: action.payload.isPageDirty
            };
        }

        case ConfigFileAddEditActionTypes.ViewCompiledYAML: {
            return {
                ...state,
                showAsCompiledYAMLEnvironment: action.payload.environment,
            };
        }
        case ConfigFileAddEditActionTypes.ViewCompiledYAMLSuccess: {
            return {
                ...state,
                isSaving: false,
                showAsCode: false,
                selectedNode: null,
                selectedConfigProperty: null,
                currentAddEditProperty: null,
                previewCode: action.payload.compiledYAML,
            };
        }

        case ConfigFileAddEditActionTypes.SaveFile: {
            return {
                ...state,
                isSaving: true
            };
        }

        case ConfigFileAddEditActionTypes.SaveFileSuccess: {
            return {
                ...state,
                isSaving: false,
                isPageDirty: false,
            };
        }

        case ConfigFileAddEditActionTypes.SaveFileFailure: {
            return {
                ...state,
                isSaving: false
            };
        }

        case ConfigFileAddEditActionTypes.OpenAddEditProperty: {
            return {
                ...state,
                isSaving: false,
                showAsCode: false,
                previewCode: null,
                showAsCompiledYAMLEnvironment: null,
                selectedNode: null,
                selectedConfigProperty: null,
                currentAddEditProperty: {
                    options: action.payload.options,
                },
                editingProperty: action.payload.options.node,
            };
        }

        case ConfigFileAddEditActionTypes.CancelAddEditProperty: {

            return {
                ...state,
                ...cancelRightPanel
            };
        }

        case ConfigFileAddEditActionTypes.SaveAddEditProperty: {
            return {
                ...state,
                ...cancelRightPanel
            };
        }
        case ConfigFileAddEditActionTypes.NodeSelected: {
            return {
                ...state,
                isSaving: false,
                previewCode: null,
                showAsCompiledYAMLEnvironment: null,
                selectedConfigProperty: null,
                currentAddEditProperty: null,
                selectedNode: action.payload.node,
                showAsCode: action.payload.node.valueType === PROPERTY_VALUE_TYPES.OBJECT,
            };
        }

        case ConfigFileAddEditActionTypes.NodeSelectedSuccess: {
            return {
                ...state,
                previewCode: action.payload.compiledYAML,
                selectedConfigProperty: action.payload.configProperty
            };
        }

        default: {
            return state;
        }
    }
}

export const getConfiguration = (state: State) => state.configuration;
export const getError = (state: State) => state.error;
export const isSaving = (state: State) => state.isSaving;
export const getEnvironments = (state: State) => state.environments;
export const getMode = (state: State) => state.inEditMode;
export const getFilePath = (state: State) => state.filePath;
export const getState = (state: State) => state;
export const getShowAsCode = (state: State) => state.showAsCode;
export const getPreviewCode = (state: State) => state.previewCode;
export const getShowAsCompiledYAMLEnvironment = (state: State) => state.showAsCompiledYAMLEnvironment;
export const getSelectedNode = (state: State) => state.selectedNode;
export const getCurrentAddEditProperty = (state: State) => state.currentAddEditProperty;
export const getSelectedConfigProperty = (state: State) => state.selectedConfigProperty;
export const getSelectedEnvironments = (state: State) => state.selectedEnvironments;
export const getIsPageDirty = (state: State) => state.isPageDirty;




