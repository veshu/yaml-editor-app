import { ConfigFile } from '../../models/config-file';
import { DashboardActionsUnion, DashboardActionTypes } from '../actions/dashboard.actions';
import * as _ from 'lodash';

export interface State {
    files: ConfigFile[];
    error: any;
    deletingFile: boolean;
    currentFilePath: string;
}

export const initialState: State = {
    files: null,
    error: null,
    deletingFile: false,
    currentFilePath: null,
};

export function reducer(state = initialState, action: DashboardActionsUnion): State {
    switch (action.type) {
        case DashboardActionTypes.LoadFiles: {
            return {
                ...state,
                files: [],
                deletingFile: false,
            };
        }

        case DashboardActionTypes.LoadFilesSuccess: {
            return {
                ...state,
                error: null,
                files: action.payload
            };
        }

        case DashboardActionTypes.LoadFilesFailure: {
            return {
                ...state,
                error: action.payload.message,
            };
        }

        case DashboardActionTypes.DeleteFile: {
            return {
                ...state,
                deletingFile: true,
                error: null,
                currentFilePath: action.payload.filePath
            };
        }

        case DashboardActionTypes.DeleteFileSuccess: {
            return {
                ...state,
                error: null,
                deletingFile: false,
                currentFilePath: null,
                files: _.filter(state.files, i => i.filename !== action.payload.filePath)
            };
        }

        case DashboardActionTypes.DeleteFileFailure: {
            return {
                ...state,
                error: action.payload.message,
                deletingFile: false,
                currentFilePath: null,
            };
        }

        default: {
            return state;
        }
    }
}

export const getFiles = (state: State) => state.files;
export const getError = (state: State) => state.error;
export const isDeletingFile = (state: State) => state.deletingFile;


