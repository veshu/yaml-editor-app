import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap, withLatestFrom } from 'rxjs/operators';
import {
    AuthActionTypes,
    Login,
    LoginFailure,
    LoginSuccess,
    LogoutSuccess,
} from '../actions/auth.actions';
import { Authenticate } from '../../models/auth';
import { FileManagementService } from '../../services/file-management.service';
import { Store, select } from '@ngrx/store';
import * as appStore from '../../store';
import { APIError } from '../actions/common.actions';

// defines the authentication effects
@Injectable()
export class AuthEffects {
    constructor(
        private actions$: Actions,
        private router: Router,
        private fileManagementService: FileManagementService,
        private store: Store<appStore.AppState>

    ) { }

    // login request effect
    @Effect()
    login$ = this.actions$.pipe(
        ofType<Login>(AuthActionTypes.Login),
        map(action => action.payload),
        exhaustMap((authInfo: Authenticate) =>
            this.fileManagementService.accessRepo(authInfo.repositoryURL, authInfo.username, authInfo.password)
                .pipe(
                    map(loginResult => new LoginSuccess({ ...loginResult, username: authInfo.username })),
                    catchError(error => of(new LoginFailure(error)))
                )
        )
    );

    // login success effect
    @Effect({ dispatch: false })
    loginSuccess$ = this.actions$.pipe(
        ofType<LoginSuccess>(AuthActionTypes.LoginSuccess),
        withLatestFrom(this.store.pipe(select(appStore.getRedirectUrl))),
        tap(([action, redirectUrl]) => {
            redirectUrl = redirectUrl || '/dashboard';
            return this.router.navigate([redirectUrl]);
        })
    );

    // login redirect effect
    @Effect({ dispatch: false })
    loginRedirect$ = this.actions$.pipe(
        ofType(AuthActionTypes.LoginRedirect, AuthActionTypes.LogoutSuccess),
        tap(() => {
            this.router.navigate(['/login']);
        })
    );

    // logout request effect
    @Effect()
    logout$ = this.actions$.pipe(
        ofType(AuthActionTypes.Logout),
        map(() => new LogoutSuccess())
    );

    // login failure effect
    @Effect()
    failure$ = this.actions$.pipe(
        ofType<LoginFailure>(AuthActionTypes.LoginFailure),
        map((error) => new APIError(error))
    );
}
