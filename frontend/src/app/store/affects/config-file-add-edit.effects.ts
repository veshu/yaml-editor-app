import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, exhaustMap, catchError, withLatestFrom, switchMap } from 'rxjs/operators';
import {
    ConfigFileAddEditActionTypes,
    PageLoad,
    PageLoadSuccess,
    PageLoadFailure,
    ViewCompiledYAML,
    ViewCompiledYAMLSuccess,
    SaveFile,
    SaveFileSuccess,
    SaveFileFailure,
    GetFileContent,
    GetFileContentFailure,
    NodeSelectedSuccess,
    NodeSelected,
    ConfigurationChange,
} from '../actions/config-file-add-edit.actions';
import { FileManagementService } from '../../services/file-management.service';
import { of } from 'rxjs';
import { Store, select } from '@ngrx/store';
import * as appStore from '..';
import * as _ from 'lodash';
import { UtilService } from '../../services/util.service';
import { Alert, APIError } from '../actions/common.actions';
import { PROPERTY_VALUE_TYPES } from '../../config';

// defines the editor page related effects
@Injectable()
export class ConfigFileAddEditEffects {
    constructor(
        private actions$: Actions,
        private store: Store<appStore.AppState>,
        private fileManagementService: FileManagementService,
        private utilService: UtilService,
    ) { }

    // handles the page load request
    @Effect()
    pageLoad$ = this.actions$.pipe(
        ofType<PageLoad>(ConfigFileAddEditActionTypes.PageLoad),
        map(action => action.payload),
        exhaustMap((data) =>
            this.fileManagementService.getEnvironments()
                .pipe(
                    map(environments => {
                        return new PageLoadSuccess({
                            environments,
                            filePath: data.filePath
                        });
                    }),
                    catchError(error => of(new PageLoadFailure(error)))
                )
        )
    );

    // page load success effect
    @Effect()
    pageLoadSuccess$ = this.actions$.pipe(
        ofType<PageLoadSuccess>(ConfigFileAddEditActionTypes.PageLoadSuccess),
        exhaustMap(() => of(new GetFileContent()))
    );

    // get file content effect
    @Effect()
    getFileContent$ = this.actions$.pipe(
        ofType<GetFileContent>(ConfigFileAddEditActionTypes.GetFileContent),
        withLatestFrom(this.store.pipe(select(appStore.getAppState))),
        exhaustMap(([action, state]) => {
            if (state.editor.filePath) {
                return this.fileManagementService.getFileContent(state.auth.currentUser.repoName, state.editor.filePath)
                    .pipe(
                        map(data => new ConfigurationChange({ configuration: data, isPageDirty: false })),
                        catchError(error => of(new GetFileContentFailure(error)))
                    );
            }
            return of(new ConfigurationChange({ configuration: {
                default: { $type: 'object' },
                environments: { $type: 'object' } }, isPageDirty: false }));
        })
    );

    // view compiled YAML effect
    @Effect()
    viewCompiledYaml$ = this.actions$.pipe(
        ofType<ViewCompiledYAML>(ConfigFileAddEditActionTypes.ViewCompiledYAML),
        withLatestFrom(this.store.pipe(select(appStore.getConfiguration))),
        switchMap(([action, config]) => {
            const selectedEnvironment = action.payload.environment;
            const defaultWithEnvironment = {};
            defaultWithEnvironment[selectedEnvironment] = { ...config.default };
            const environmentJson = {};
            environmentJson[selectedEnvironment] = { ...(config.environments[selectedEnvironment] || {}) };
            const compiledJSON = _.merge({}, defaultWithEnvironment, environmentJson);
            const compiledYAML = this.utilService.convertJsonToYaml(compiledJSON);
            return of(new ViewCompiledYAMLSuccess({ compiledYAML }));
        }),
    );

    // node selected effect to show detail
    @Effect()
    nodeSelected$ = this.actions$.pipe(
        ofType<NodeSelected>(ConfigFileAddEditActionTypes.NodeSelected),
        map(action => action.payload),
        exhaustMap(data => {
            if (data.node.valueType === PROPERTY_VALUE_TYPES.OBJECT) {
                const compiledYAML = this.utilService.convertJsonToYaml(data.node.jsonValue);
                return of(new NodeSelectedSuccess({ compiledYAML, configProperty: null }));
            } else {

                const configProperty = data.node.toConfigProperty();
                return of(new NodeSelectedSuccess({ compiledYAML: null, configProperty }));
            }
        })
    );

    // save file effect
    @Effect()
    saveFile$ = this.actions$.pipe(
        ofType<SaveFile>(ConfigFileAddEditActionTypes.SaveFile),
        withLatestFrom(this.store.pipe(select(appStore.getAppState))),
        switchMap(([action, data]) => {
            if (data.editor.inEditMode) {
                const commitMessage = `${data.auth.currentUser.username} updates the file ${data.editor.filePath}`;
                return this.fileManagementService.updateFile(data.auth.currentUser.repoName,
                    data.editor.filePath, data.editor.configuration, commitMessage)
                    .pipe(
                        map(() => new SaveFileSuccess({ inEditMode: data.editor.inEditMode })),
                        catchError(error => of(new SaveFileFailure(error)))
                    );
            } else {
                const filePath = action.payload.filePath;
                const commitMessage = `${data.auth.currentUser.username} creates the file ${filePath}`;
                return this.fileManagementService.createNewFile(data.auth.currentUser.repoName,
                    filePath, data.editor.configuration, commitMessage)
                    .pipe(
                        map(() => new SaveFileSuccess({ inEditMode: data.editor.inEditMode })),
                        catchError(error => of(new SaveFileFailure(error)))
                    );
            }
        })
    );

    // save file success effect
    @Effect()
    saveFileSuccess$ = this.actions$.pipe(
        ofType<SaveFileSuccess>(ConfigFileAddEditActionTypes.SaveFileSuccess),
        map(action => action.payload),
        exhaustMap(data => {
            let message = '';
            let editorType = '';
            if (data.inEditMode) {
                message = 'The file is updated successfully.';
                editorType = 'edit';
            } else {
                message = 'The file is created successfully.';
                editorType = 'add';
            }
            return of(new Alert({ message, editorType }));
        }),
    );

    // save file failure effect
    @Effect()
    saveFailure$ = this.actions$.pipe(
        ofType<SaveFileFailure>(ConfigFileAddEditActionTypes.SaveFileFailure),
        map((error) => new APIError(error))
    );

    // get file failure effect
    @Effect()
    getFileFailure$ = this.actions$.pipe(
        ofType<GetFileContentFailure>(ConfigFileAddEditActionTypes.GetFileContentFailure),
        map((error) => new APIError(error))
    );

    // load environment failure effect
    @Effect()
    pageLoadFailure$ = this.actions$.pipe(
        ofType<PageLoadFailure>(ConfigFileAddEditActionTypes.PageLoadFailure),
        map((error) => new APIError(error))
    );
}
