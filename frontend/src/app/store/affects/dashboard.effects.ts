import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap, withLatestFrom, switchMap } from 'rxjs/operators';
import {
    LoadFiles, DashboardActionTypes, LoadFilesSuccess, LoadFilesFailure, DeleteFileFailure, DeleteFileSuccess, DeleteFile
} from '../actions/dashboard.actions';
import { User } from '../../models/auth';
import { FileManagementService } from '../../services/file-management.service';
import { Store, select } from '@ngrx/store';
import * as appStore from '..';
import { Alert, APIError } from '../actions/common.actions';

// defines the dashboard page related effects
@Injectable()
export class DashboardEffects {
    constructor(
        private actions$: Actions,
        private fileManagementService: FileManagementService,
        private store: Store<appStore.AppState>

    ) { }

    // load files effect
    @Effect()
    loadFiles$ = this.actions$.pipe(
        ofType<LoadFiles>(DashboardActionTypes.LoadFiles, DashboardActionTypes.DeleteFileFailure),
        withLatestFrom(this.store.pipe(select(appStore.getRepositoryName))),
        switchMap(([action, repositoryName]) =>
            this.fileManagementService.getFiles(repositoryName)
                .pipe(
                    map(result => new LoadFilesSuccess(result)),
                    catchError(error => of(new LoadFilesFailure(error)))
                )
        )
    );

    // delete file effect
    @Effect()
    deleteFile$ = this.actions$.pipe(
        ofType<DeleteFile>(DashboardActionTypes.DeleteFile),
        withLatestFrom(this.store.pipe(select(appStore.getRepositoryName))),
        switchMap(([action, repositoryName]) =>
            this.fileManagementService.deleteFile(repositoryName, action.payload.filePath)
                .pipe(
                    map(() => new DeleteFileSuccess({ filePath: action.payload.filePath })),
                    catchError(error => of(new DeleteFileFailure(error)))
                )
        )
    );

    // delete file success effect
    @Effect()
    deleteFileSuccess$ = this.actions$.pipe(
        ofType<DeleteFileSuccess>(DashboardActionTypes.DeleteFileSuccess),
        map(() => new Alert({ message: 'The file is deleted successfully.' , editorType: 'delete'}))
    );

    // delete file failure effect
    @Effect()
    failure$ = this.actions$.pipe(
        ofType<DeleteFileFailure>(DashboardActionTypes.DeleteFileFailure),
        map((error) => new APIError(error))
    );

    // load files failure effect
    @Effect()
    loadFailure$ = this.actions$.pipe(
        ofType<LoadFilesFailure>(DashboardActionTypes.LoadFilesFailure),
        map((error) => new APIError(error))
    );
}
